SET NAMES 'utf8';

-- --------------------------------------------------------
-- Prefix setting
--
SET  @tablePrefix = "adl_";

-- --------------------------------------------------------

--
-- Structure de la table `campaign`
--
SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "campaign",
" ( `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `start` date NOT NULL,
  `end` date NOT NULL,
  `is_message` int(11) NOT NULL DEFAULT '0',
  `messagehtml` text NOT NULL,
  `is_icon` int(11) NOT NULL DEFAULT '0',
  `iconhtml` text NOT NULL,
  `is_tag` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) NOT NULL,
  `tag_messagehtml` text NOT NULL,
  `tag_explainhtml` text NOT NULL,
  `tag_explainstart` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `start` (`start`,`is_message`,`is_icon`,`is_tag`,`tag`,`tag_explainstart`)
) DEFAULT CHARSET=utf8 ;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "events",
" (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `description` text NOT NULL,
  `start_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `address` varchar(255) default NULL,
  `city` varchar(255) NOT NULL default '',
  `department` int(11) default NULL,
  `region` int(11) NOT NULL default '0',
  `postalcode` int(11) default NULL,
  `latitude` float default NULL,
  `longitude` float default NULL,
  `locality` int(11) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  `contact` varchar(255) NOT NULL default '',
  `submitter` varchar(255) NOT NULL default '',
  `moderated` int(11) NOT NULL default '0',
  `secret` varchar(255) NOT NULL default '',
  `decision_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `submission_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`),
  KEY `region` (`region`),
  KEY `moderated` (`moderated`),
  KEY `start_time` (`start_time`),
  KEY `end_time` (`end_time`),
  KEY `department` (`department`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `localgroups`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "localgroups",
" (
  `id` int(11) NOT NULL auto_increment,
  `region` int(11) NOT NULL default '0',
  `department` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `city` varchar(255) NOT NULL default '',
  `address` varchar(255) default NULL,
  `contact` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `mail` varchar(255) default NULL,
  `comment` text,
  `latitude` float default NULL,
  `longitude` float default NULL,
  `postalcode` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `region` (`region`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "notes",
" (
    `id` int(11) NOT NULL auto_increment,
    `contents` text NOT NULL,
    `date` datetime NOT NULL default '0000-00-00 00:00:00',
    `event_id` int(11) default NULL,
    `author_id` int(11) default NULL,
    PRIMARY KEY (`id`)
);");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "regions",
" (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `region_department`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "region_department",
" (
  `region_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  UNIQUE KEY `department` (`department_id`),
  KEY `region_id` (`region_id`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "tags",
" (
  `id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `tags_categories`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "tags_categories",
" (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `tags_events`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "tags_events",
" (
  `event_id` int(11) NOT NULL default '0',
  `tag_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`event_id`,`tag_id`),
  KEY `tag_id` (`tag_id`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

SET @s = CONCAT("CREATE TABLE IF NOT EXISTS ", @tablePrefix, "users",
" (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `firstname` varchar(255) NOT NULL default '',
  `lastname` varchar(255) NOT NULL default '',
  `admin` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;");
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;



