<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");
include_once("inc/class.event.inc.php");
include_once("inc/class.export.inc.php");

$event_id = get_safe_integer('id', 0);

$eventList = new exportEvent($db);
$eventList->map = get_safe_integer('map', 0);

echo $eventList->rssStartCalendar();

/* Generate item list */
if ($event_id==0)
  {
    $eventList->region = get_safe_integer('region', 'all');
    $eventList->count = get_safe_integer('count', 10);
    $eventList->daylimit = get_safe_integer('daylimit', 30);
    $tag = get_safe_string('tag', '');
    $eventList->tags = ($tag>'' ? explode(' ', $tag) : array());
    $list = $eventList->getEventsList();
  }
else
  {
    $sql = "SELECT events.id as id ".
      "FROM {$GLOBALS['db_tablename_prefix']}events AS events ".
      "WHERE id=". $db->quote_smart($event_id). " AND (moderated=1)";
    $list = $db->query($sql);
  }

if ($list == FALSE)
{
  error ("Erreur lors de la récupération des évènements");
  exit;
}

while ($eventList->event->id = $db->getOne($list))
{
    echo $eventList->rssEventHeaderCalendar();
}
echo $eventList->rssEndHeaderCalendar();


/* Generate items */
if ($event_id==0)
  $list = $eventList->getEventsList();
else
  {
    $list = $db->query($sql);
  }

if ($list == FALSE)
{
  echo "Erreur lors de la récupération des évènements";
  exit;
}

while ($eventList->event->id = $db->getOne($list))
{
  $eventList->event->get();
  echo $eventList->event->toRss($map);
} // end while

echo $eventList->rssEndCalendar();
