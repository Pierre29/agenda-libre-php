<?php

/*
 * Copyright 2005-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/**@file
 *
 * Main page of the Agenda that displays the calendar
 */

include("inc/main.inc.php");

put_header("Accueil");

if ($adl_summary>"" && !isset($portail))
  echo "<h5>$adl_summary</h5>\n";

$region = get_safe_integer('region', 'all');

/*
 * Compute the month to be displayed in the agenda
 */
if(isset($_GET['year']) && ! isset($_GET['month']) )
{
  if (preg_match("~^[0-9]{4}$~", $_GET['year']))
    $year = $_GET['year'];
  else
    $year = date("Y");

  year_calendar ($db, $year, $region);
}
else if (isset($_GET['month']) && isset($_GET['year']))
{
  if (preg_match("~^[0-9]{1,2}$~", $_GET['month']) // month is N or NN
      && preg_match("~^[0-9]{4}$~", $_GET['year']) // year is NNNN
      && 1 <= $_GET['month'] && $_GET['month'] <= 12) {
    $month = $_GET['month'];
    $year = $_GET['year'];
  } else {
    $year = date("Y");
    $month = date("n");
  }
  one_month_calendar ($db, $month, $year, $region);
}
else
{
  $year = date("Y");
  $month = date("n");
  one_month_calendar ($db, $month, $year, $region);
}

put_footer();
?>
