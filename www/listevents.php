<?php

/*
 * Copyright 2007-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.event.inc.php");
include('inc/class.export.inc.php');

function list_events($events)
{
  global $db;
  $event = new event($db);

  echo " <ul>\n";
  while($event->id = $db->getOne($events))
    {
	  $event->get();
      echo "<li>";
      echo "<a href=\"showevent.php?id=" . $event->id . "\">";
      echo stripslashes($event->title);
      echo "</a>";
      $startday = onlyday_timestamp2humanreadable(date_mysql2timestamp($event->start_time));
      $endday   = onlyday_timestamp2humanreadable(date_mysql2timestamp($event->end_time));
      echo "<br/>";
      if ($startday == $endday)
	    echo "le " . $startday . " ";
      else
	    echo "du " . $startday . " au " . $endday . " ";
      echo " à " . $event->city;
      echo "</li>";
    }
  echo " </ul>\n";
}

put_header("Liste d'évènements");

/* Fetch region name */
$region_num = get_safe_integer('region', 'all');
if ($region_num != "all")
  $region = region_find($db, $region_num);
else
  $region = $adl_all_region;

$daylimit = get_safe_integer('daylimit', 0);

// Get tags ===
$tag = get_safe_string('tag', '');
$aTags = ($tag>'' ? explode(' ', $tag) : array());

echo "<h2>Les évènements <i>" . $tag . "</i></h2>\n";
$hasevent = FALSE;

$eventList = new exportEvent($db);
$eventList->region = $region_num;
$eventList->daylimit = $daylimit;
$eventList->tags = $aTags;
$events = $eventList->getEventsList();

if (! $events)
{
  error (_("Erreur lors de la requête SQL."));
  put_footer();
  exit;
}

// display exports url
echo "<div class='list-group'>Ces événements en flux ";
echo "<a class='list-group-items' href=\"". $eventList->rssUrl(). "\"><i class='fa fa-rss'></i>&nbsp;RSS</a>&nbsp; ";
echo "<a class='list-group-items' href=\"ical.php?tag=" . $tag ."\"><i class='fa fa-calendar'></i>&nbsp;iCal</a>&nbsp; ";
echo "<a class='list-group-items' href=\"javascript.php?tag=" . $tag ."\"><i class='fa fa-connectdevelop'></i>&nbsp;JavaScript</a>&nbsp; ";
echo "<a class='list-group-items' href=\"http://www.google.com/calendar/render?cid=". calendar_absolute_url("ical.php?tag=". 
	$tag). "\"><i class='fa fa-google'></i>&nbsp;calendrier Google</a>";
echo "</p>";

if ($db->numRows($events))
{
  $hasevent = TRUE;
  echo "<p>";
  echo "<b>Prochainement</b>";
  if ($db->numRows($events) == 1)
    echo ", un évènement&nbsp;:";
  else
    echo ", " . $db->numRows($events) . " évènements&nbsp;:";
  echo "</p>";
  
  // display event list
  list_events($events);
}


// Dans le passé
$eventList->past = true;
$events = $eventList->getEventsList();

if (! $events)
{
  error (_("Erreur lors de la requête SQL."));
  put_footer();
  exit;
}

if ($db->numRows($events))
{
  $hasevent = TRUE;
  echo "<p>";
  echo "<b>Dans le passé</b>";
  if ($db->numRows($events) == 1)
    echo ", un évènement&nbsp;:";
  else
    echo ", " . $db->numRows($events) . " évènements&nbsp;:";
  echo "</p>";
  list_events($events);
}

if (! $hasevent)
  echo "<p>Aucun évènement avec ce tag.</p>";

put_footer();
?>

