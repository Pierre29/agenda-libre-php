<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header ("Soumission d'évènement");

?>

<h2>Soumission d'évènement par script</h1>

<h3>Table des matières</h3>

<ul>
 <li><a href="#principe">Principe</a></li>
 <li><a href="#utilisation">Utilisation</a></li>
 <li><a href="#qa">Questions ? Problèmes ?</a></li>
</ul>

<a name="principe"></a>
<h3>Principe</h3>

<p>Afin de faciliter la soumission d'évènement régulier, un
petit script vous est proposé. À partir de la description d'un
évènement dans un fichier XML et d'informations données sur
la ligne de commande, il permet de soumettre automatiquement un
évènement dans l'Agenda du Libre.</p>

<p>Ce script est disponible <a href="http://gitorious.org/agenda-du-libre-php/mainline/blobs/raw/master/scripts/adl-submit.py">ici</a>.
Il utilise Python, et le module Curl pour Python (paquets Debian
<i>python2.3</i> et <i>python2.3-pycurl</i> ou <i>python2.4</i> et
<i>python2.4-pycurl</i>.</p>

<a name="utilisation"></a>
<h3>Utilisation</h3>

<p>Il faut tout d'abord rédiger un document XML en prenant exemple
sur <a href="http://gitorious.org/agenda-du-libre-php/mainline/blobs/raw/master/scripts/event.xml">celui-ci</a>.
Ce fichier XML doit contenir toutes les informations qui ne changent
pas sur votre évènement. Les champs existants sont&nbsp;:</p>

<ul>

 <li><b>title</b>, le titre de l'évènement, sans le nom du lieu
 ni la date. Il doit faire en général moins de cinq
 mots&nbsp;;</li>

 <li><b>start-date</b>, la date de début au format
 <code>AAAA-MM-JJ</code>&nbsp;;</li>

 <li><b>end-date</b>, la date de fin au format
 <code>AAAA-MM-JJ</code>&nbsp;;</li>

 <li><b>start-hour</b>, l'heure de début au format
 <code>HH:MM</code>&nbsp;;</li>

 <li><b>end-hour</b>, la fin de début au format
 <code>HH:MM</code>&nbsp;;</li>

 <li><b>description</b>, une description la plus complète
 possible. Elle peut contenir du formatage en HTML en utilisant les
 balises <i>a</i> (liens), <i>b</i> (gras), <i>i</i> (italique),
 <i>ul</i> et <i>li</i> (liste). Le texte de la description peut
 également contenir la chaîne <code>$month</code>, qui sera
 automatiquement remplacée par le mois du début de l'évènement, ou la
 chaîne <code>$date</code> qui sera remplacée par la date du début de
 l'évènement&nbsp;;</li>

 <li><b>city</b>, la ville où a lieu l'évènement&nbsp;;</li>

 <li><b>region</b>, la région où a lieu
 l'évènement&nbsp;;</li>

 <li><b>url</b>, un lien direct vers une page donnant des informations
 complémentaires sur l'évènement&nbsp;;</li>

 <li><b>contact</b>, une adresse e-mail de contact&nbsp;;</li>

 <li><b>submitter</b>, adresse e-mail de la la personne ayant proposé
 l'information (prend la valeur de <b>contact</b> si
 absent)&nbsp;;</li>

 <li><b>tags</b>, liste de tags séparés par des espaces.</li>
 </ul>

<p>Une fois ce fichier écrit, il suffit d'utiliser le script. Tout,
d'abord, on passera un paramètre <code>--file event.xml</code> pour
spécifier le fichier XML précédement créé. On passera
également en paramètre toutes les informations manquantes dans
le fichier XML. Les options ont le même nom que les champs du
fichier XML. Si une option est fournie alors que le champ existe dans
le fichier XML, alors la valeur de l'option sera préférée
à la valeur du fichier XML. Au total, entre les options de la ligne
de commande et les valeurs du fichier, tous les champs doivent être
remplis pour que la soumission fonctionne. Exceptions&nbsp;: si le
champ <b>end-date</b> n'est pas spécifié, alors il prendra la valeur
du champ <b>start-date</b> et si le champ <b>submitter</b> n'est pas
spécifié, alors il prendra la valeur du champ <b>contact</b>.</p>

<p>Pour un évènement régulier (une fois par mois, par semaine), une
utilisation typique sera de renseigner les champs <i>title</i>,
<i>start-hour</i>, <i>end-hour</i>, <i>description</i>, <i>city</i>,
<i>region</i>, <i>url</i>, <i>contact</i>, <i>submitter</i> et
<i>tags</i> dans le fichier XML. On spécifiera alors la date sur la
ligne de commande.</p>

<p>On peut tout d'abord vérifier que le formatage de la description de
l'évènement est correct, en utilisant l'option
<code>--test-output</code>. On peut ainsi générer un fichier HTML, que
l'on pourra valider avec un navigateur Web.</p>

<pre>
./adl-submit.py --file event.xml --start-date 2006-01-23
                --test-output test.html
</pre>

<p>Une fois ceci fait, il reste plus qu'à soumettre
l'évènement&nbsp;:</p>

<pre>
./adl-submit.py --file event.xml --start-date 2006-01-23
</pre>

<a name="qa"></a>
<h3>Questions ? Problèmes ?</h3>

<p>Si vous avez des questions concernant son utilisation, vous pouvez
contacter la liste de développement de l'Agenda du Libre, en vous
<a
href="http://www.agendadulibre.org/cgi-bin/mailman/listinfo/devel">inscrivant
ici</a>.</p>

<?php put_footer(); ?>
