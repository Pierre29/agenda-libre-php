<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");
include("inc/class.event.inc.php");


if (isset($_POST['__event_rejection_cancel']))
{
  header("Location: moderation.php");
  exit;
}
else if (isset($_POST['__event_rejection_confirm']))
{
  $id = get_safe_integer('id', 0);

  switch ($_POST['reason'])
  {
    case "out-of-subject" :
      $reason = wordwrap($GLOBALS["adl_notrelated_message"]); break;
    case "not-enough-infos" :
      $reason = wordwrap($GLOBALS["adl_notenough_message"]); break;
    case "duplicate" :
      $reason = wordwrap($GLOBALS["adl_double_message"]); break;
    case "custom" :
      $reason = wordwrap("Toutefois, votre évènement n'a".
        " pour le moment pas retenu l'attention des modérateurs,".
        " pour la raison suivante : " . stripslashes($_POST['customtext']));
        break;
    case "hidden" :
      $reason = "";
  }
  $event = new event($db, $id);
  if ($event->error)
    {
      put_header("Rejet d'un évènement");
      error($event->message);
      put_footer();
      exit;
    }

  $event->delete();
  if ($event->error)
  {
      put_header("Rejet d'un évènement");
      error($event->message);
      put_footer();
      exit;
  }

  if ($reason != "")
    {

      /* Send email */
      $mailBody =
         "Bonjour,\n\n" .
         "Vous avez soumis l'évènement suivant dans {$adl_title}, et nous\n" .
         "vous remercions de cette contribution.\n\n" .
         $reason .
         "%s".
         "\n\nPour rappel, voici le contenu de votre évènement:\n" .
         "=====================================================\n" .
         $event->formatAscii() . "\n" .
         "=====================================================\n\n" .
         "Avec tous nos remerciements pour votre contribution,\n\n" .
         "-- \nL'équipe de modération";

      $mailBodySubmitter = sprintf($mailBody,
         "\n\nPour toute réclamation, n'hésitez pas à contacter l'équipe de modérateurs\n" .
         "à l'adresse ". scramble_email($GLOBALS["moderatorlist"]). ".");

      if (substr($event->submitter,0,4) != "http")
        {
          calendar_mail ($event->submitter,
            "Évènement '" . stripslashes($event->title) . "' refusé",
            $mailBodySubmitter,
            $event->id);
        }

      calendar_mail ($moderatorlist,
        "Évènement '" . stripslashes($event->title) . "' refusé",
        sprintf($mailBody, ""),
        $event->id);
    }

  header("Location: moderation.php");
  exit;
}

$id = get_safe_integer('id', 0);

put_header("Rejet d'un évènement");

$event = new event($db, $id);
if ($event->error)
{
  error($event->message);
  put_footer();
  exit;
}

if ($event->moderated)
{
  echo "<p>Évènement déjà modéré</p>";
  put_footer();
  exit;
}

echo '<p class="moderationheader">';
echo "<a href=\"moderation.php\">Modération</a>&nbsp;&gt;&gt;&gt&nbsp;Rejet de l'évènement";
echo "</p>";

echo '<div class="moderationbox">';
echo '<form action="rejectevent.php?id=' . $id . '" method="post">';
echo '<p style="text-align: center;">Quel motif souhaitez-vous associer au rejet de cet évènement&nbsp;?</p>';
echo '<p>';
echo '<label for="aa">Hors sujet <input id="aa" type="radio" name="reason" value="out-of-subject" onClick="hidetextarea();" /></label><br/>';
echo '<label for="ab">Pas assez d\'informations <input id="ab" type="radio" name="reason" value="not-enough-infos" onClick="hidetextarea();" /></label><br/>';
echo '<label for="ac">&Eacute;vènement déjà enregistré <input id="ac" type="radio" name="reason" value="duplicate" onClick="hidetextarea();" /></label><br/>';
echo '<label for="ad">Raison spécifique (précisez) <input id="ad" type="radio" name="reason" value="custom" onClick="document.getElementById(\'customtext\').style.display = \'block\';" /></label><br/>';
echo '<textarea id="customtext" style="display: block;" cols="80" rows="10" name="customtext"></textarea>';
echo '<script language=\'javascript\'>function hidetextarea() { document.getElementById(\'customtext\').style.display=\'none\'; } hidetextarea();</script>';
echo '<label for="ae">Spam ou test - ne pas avertir <input id="ae" type="radio" name="reason" value="hidden" checked="checked" onClick="hidetextarea();" /></label><br/>';
echo '</p>';
echo '<input name="__event_rejection_confirm" type="submit" value="Rejeter"/>&nbsp;';
echo '<input name="__event_rejection_cancel" type="submit" value="Annuler"/>';
echo '</form>';
echo '</div>';

echo '<div class="moderationbox">';
echo $event->formatHTML();
echo '</div>';

put_footer();

?>
