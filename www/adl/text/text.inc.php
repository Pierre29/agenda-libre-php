<?php

/*
 * Copyright 2007-2015
 * - Melanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

// From funcs.inc.php

$adl_title = "Mon Agenda";
$adl_title_iframe = "Agenda des événements liés à Mon Domaine";
$adl_subtitle = "";
$adl_short_title = "Mon Agenda";
$adl_all_region = "toutes les régions";

$adl_url_contact = "agenda CHEZ listes POINT exemple POINT org";

$adl_mailto_moderates = "agenda@exemple.org";

$adl_form_description_guide = "<i>Les balises HTML autorisées sont ".
  "&lt;p&gt;, &lt;b&gt;, &lt;i&gt;, &lt;ul&gt;, &lt;ol&gt;, &lt;li&gt;, &lt;br/&gt;, &lt;a&gt;.".
  "Merci d'utiliser ces balises pour formater la description de votre évènement.</i><br/> ".
  "<i>Veillez à utiliser les balises <code>&lt;p&gt;</code> pour formater les paragraphes, ".
  "et non la balise &lt;br/&gt;.</i><br/>";

$adl_form_tags_guide = "<i>Mots-clés pour l'évènement. Les mots-clés sont séparés par des ".
  "espaces. Un mot-clé ne peut contenir que des lettres minuscules ".
  "sans accents, des chiffres et des tirets.</i><br/>\n".
  "Dans les mots-clés, indiquez le nom de la ou des associations ".
  "organisatrices. N'indiquez pas le nom de la ville ou de la ".
  "région.<br/>\n";

$adl_locality = array(-1=>"locale", 0=>"régionale", 1=>"nationale", 2=>"internationale");

// ical.php
$adl_uid_end = "agenda.exemple.org";
$adl_one_event = "Un évènement de Mon Agenda";

// icallist.php
$adl_ical_intro = "<p>Chaque calendrier iCal liste les évènements pour les 30 prochains jours ".
  "en cours dans une région donnée. En vous inscrivant au calendrier de votre région, vous verrez ".
  "apparaître les évènements à portée locale de votre région, mais également les évènements ".
  "à portée nationale et internationale.</p>";
$adl_mailto_devel = "mailto:agenda-devel AT exemple.org";
$adl_ical_more = "<p>Quelques fonctionnalités additionnelles des calendriers iCal&nbsp;:</p>".
  "<ul>\n".
  "<li>Vous pouvez limiter les évènements d'un calendrier iCal à un certain ".
  "mot-clé, en passant le paramètre <code>tag</code>. Cela permet par ".
  "exemple de récupérer un calendrier iCal des évènements organisés uniquement ".
  "par votre association, à partir du moment où vous pensez à marquer ".
  "tous vos évènements avec un mot-clés précis.<br/>\n".
  "Exemple&nbsp;: <code>http://{$adl_uid_end}/ical.php?tag=formation</code>. ".
  "</li>\n".
  "</ul>\n";

// index.php
$adl_summary = "Agenda des événements liés à Mon Domaine";

// moderate.php
$adl_notrelated_message = "Toutefois, l'évènement proposé n'a pour l'instant pas retenu ".
  "l'attention des modérateurs. En effet, l'évènement proposé ne concerne pas Mon Domaine, ".
  "ou bien le lien avec Mon Domaine n'est pas évident dans la formulation ".
  "actuelle, ou alors il s'agit d'un évènement ou d'une formation payante et coûteuse. ".
  "Si l'évènement concerne vraiment Mon Domaine et qu'il ne s'agit pas d'une ".
  "formation payante, n'hésitez pas à le soumettre à nouveau avec une description plus".
  " claire.";
$adl_notenough_message = "Votre évènement a tout à fait sa place dans l'Agenda, ".
  "mais les modérateurs trouvent que la description de celui-ci n'est ".
  "pas tout à fait assez complète pour être validée.\n\nLa description doit être ".
  "compréhensible par un nouveau venu dans le domaine, et doit donc ".
  "préciser le principe de la rencontre, le public visé, la rôle du ou des ".
  "associations organisatrices ou bénéficiaires, etc. Même s'il s'agit d'une rencontre ".
  "régulière, n'hésitez pas à répéter à chaque fois ces informations, elles ".
  "sont importantes.\n\nNous vous invitons donc vivement à soumettre à nouveau ".
  "cet évènement avec une description plus complète.";
$adl_double_message = "L'évènement que vous proposez est déjà présent dans ".
  "l'Agenda.\n\n";

// rss.php
$adl_url = "http://localhost";

// exportlist.php
$adl_export_intro = "<p>Chaque flux RSS liste les évènements pour les 30 prochains jours en cours dans une région donnée. En vous abonnant à un flux régional, vous recevrez des informations sur les évènements à portée locale, mais également sur les évènements à portée nationale et internationale.</p>";
$adl_rss_more = "<p>Quelques fonctionnalités intéressantes des flux RSS&nbsp;:</p>\n".
  "<ul>\n".
  "<li>Vous pouvez filtrer le flux en sélectionnant des évènements par ".
  "mots-clés avec le paramètre <code>tag</code>'. Par exemple, en marquant ".
  "tous les évènements d'un espace avec le même mot-clé, vous pouvez afficher le ".
  "flux RSS des évènements organisés uniquement par cet espace.<br /> ".
  "Exemple&nbsp;: <code>". calendar_absolute_url('rss.php?tag=formation'). "</code>. ".
  "</li>\n ".
  "<li>Vous pouvez modifier la limite aux 30 prochains jours des flux ".
  "RSS en utilisant le paramètre <code>daylimit</code>.<br/> ".
  "Exemple&nbsp;: <code>$adl_url/rss.php?region=6&amp;daylimit=42</code> ".
  "</li>\n</ul>\n";

// javascriptlist.php
$adl_js_intro = "<p>Chaque javascript liste les 10 prochains évènements à venir dans une région donnée. ".
   "En recopiant le code javascript pour une région, vous recevrez des informations sur les évènements à portée locale, ".
   "mais également sur les évènements à portée nationale et internationale.</p>";
$adl_js_more = "<p>Quelques fonctionnalités intéressantes du widget Javascript&nbsp;:</p>\n".
  "<ul>\n".
  "<li>Vous pouvez obtenir plus ou moins d'événements dans votre liste ".
  " en ajoutant le paramètre <code>count</code><br /> ".
  "Exemple&nbsp;: <code>". calendar_absolute_url('javascript.php?region=10&count=20'). "</code>. ".
  "</li>\n</ul>\n".
  "<h5>Ci-dessous, vous pouvez voir le résultat de votre sélection&nbsp;:</h5>";

// showevents.php
$adl_localgroups_around = "Groupes d'utilisateurs alentours";
$adl_localgroups_acronym = "GUL";

// showlocalgroup.php
$adl_localgroups_info = "Information sur un groupe d'utilisateurs";

// submit.php
$adl_submit_summary = "<p>L'événement proposé n'apparaîtra dans l'Agenda qu'après ".
  "sa validation par un modérateur. Vous en serez informé par un courrier ".
  "électronique envoyé à l'adresse e-mail de contact donnée ci-dessous.</p>\n".
  "<p>Si vous soumettez régulièrement un évènement récurrent dans ".
  "l'Agenda, vous pouvez automatiser cette procédure à ".
  "l'aide d'un script <a href='submit-script-doc.php'>script que nous vous ".
  "proposons</a>.</p>\n";

$adl_submit_advises = "<h1>Recommandations importantes</h1>\n".
  //"<p>Si vous soumettez régulièrement un évènement récurrent dans ".
  //"l'Agenda, vous pouvez automatiser cette procédure à ".
  //"l'aide d'un <a href=\"submit-script-doc.php\">script que nous vous ".
  //"proposons</a>.</p>\n".
  "<ul>\n".
  "<li>L'évènement doit concerner <b>Mon Domaine</b>, ".
  "qu'il s'adresse aux professionnels, aux usagers de Mon Domaine ou au grand public : ".
  "rencontre séminaire, journée porte ouverte... ".
  "L'agenda n'a pas vocation à publier d'autres types ".
  "d'informations.</li>\n".
  "<li>Le texte doit donner une description de l'évènement, rédigé en français, ".
  "en évitant le style télégraphique, les fautes de grammaire et d'orthographe. ".
  "Vous pouvez utiliser les balises HTML pour les caractères gras, italiques, ".
  "les paragraphes, les liens. N'écrivez pas l'intégralité des mots en ".
  "majuscules, même pour le titre ou la ville.</li>".
  "<li>Pensez que le lecteur peut ne pas connaître votre espace ou votre ".
  "organisation, les usages dont il sera question, etc.. Mentionnez les ".
  "précisions nécessaires : nature de l'évènement, public visé, lieu exact, ".
  "date, heure, même si vous avez déjà soumis un évènement analogue dans ".
  "l'Agenda, lien vers une page internet donnant plus d'informations sur ".
  "l'évènement.</li>";
  "</ul>\n";

return;
?>
