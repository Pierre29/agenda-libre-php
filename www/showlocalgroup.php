<?php

/*
 * Copyright 2004-2015
 * - Loic Dayot <ldayot A ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include_once("inc/class.region.inc.php");

$localgroup_id = get_safe_integer('id', 0);
$localgroup_res = $db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}localgroups WHERE id='{$localgroup_id}'");
$localgroup = $db->fetchObject($localgroup_res);

if (! $localgroup)
{
// end map
if ($localgroup->latitude>0)
  put_header($adl_localgroup_info);
  echo "<p class=\"error\">Pas de ". $adl_localgroups_acronym. " avec cet identifiant.</p>";
  put_footer();
  exit;
}


// end map
if ($localgroup->latitude>0)
  {
    // start map
    $jcode = "
    <script src=\"http://openlayers.org/api/OpenLayers.js\"></script>
    <script src=\"http://openstreetmap.org/openlayers/OpenStreetMap.js\"></script>
    <script type=\"text/javascript\">
        var lat={$localgroup->latitude}
        var lon={$localgroup->longitude}
        var zoom=11
        var map;
        function init() {
            map = new OpenLayers.Map (\"map\", {
                controls:[
                    new OpenLayers.Control.Navigation(),
                    new OpenLayers.Control.PanZoomBar(),
                    new OpenLayers.Control.Attribution()],
                    maxResolution: 156543.0399,
                    units: 'm',
            } );

            map.addControl(new OpenLayers.Control.LayerSwitcher());

            layerTilesAtHome = new OpenLayers.Layer.OSM.Mapnik(\"Mapnik\");
            map.addLayer(layerTilesAtHome);

            var lonLat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection(\"EPSG:4326\"), new OpenLayers.Projection(\"EPSG:900913\"));

            map.setCenter (lonLat, zoom);

            var newl = new OpenLayers.Layer.Text('". $adl_localgroups_acronym. "', {location: '" . calendar_absolute_url("localgrouptextlist.php?id={$localgroup_id}") . "'});
            map.addLayer(newl);

        }

    </script>\n";
    put_header($adl_localgroups_info, $jcode, "init();");
  }
else
  {
    put_header($adl_localgroups_info);
  }

function format_localgroup ($db, $localgroup)
{
  $name        = stripslashes($localgroup->name);
  $oRegion = new region($db);
  $oRegion->getRegionFromDepartment($localgroup->department);
  $region      = stripslashes($oRegion->get());
  $city        = stripslashes($localgroup->city);
  $address     = stripslashes($localgroup->address);
  $postalcode  = stripslashes($localgroup->postalcode);
  $comment     = stripslashes($localgroup->comment);
  $url         = stripslashes($localgroup->url);
  $contact     = stripslashes($localgroup->contact);
  $mail        = scramble_email(stripslashes($localgroup->mail));
  $phone       = stripslashes($localgroup->phone);

  $result  = "<h2><i>" . $city . "</i> : " . $name . "</h2>\n\n";
  $result .= "<h3>Localisation</h3>\n";
  $result .= "<p>$address - $postalcode <i><a href=\"http://fr.wikipedia.org/wiki/" . strtolower($city) . "\">"
            . $city . "</a></i> - <a href=\"http://fr.wikipedia.org/wiki/"
            . $region . "\">" . $region . "</a></p>\n";

  $result .= "<h3>Description</h3>\n";
  $result .= "$comment\n";

  $result .= "<h3>Informations</h3>\n";
  $result .= "<p>Site Web: <a href=\"" . $url . "\">" . $url . "</a></p>\n";
  $result .= "<p>Contact: $contact - <a href=\"mailto:" . $mail . "\">" . $mail . "</a> - $phone</p>\n";

  return $result;
}


echo format_localgroup ($db, $localgroup);

// start map
if ($localgroup->latitude>0)
  echo "<div style=\"margin: auto; width:75%; height:400px; border: 1px solid black;\" id=\"map\"></div>\n";
// end map

put_footer();

?>
