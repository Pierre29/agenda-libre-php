<?php

/*
 * Copyright 2004-2015
 * - Melanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.event.inc.php");

put_header("Soumettre un évènement");

echo "<h2>Soumettre un évènement</h2>";

$event = new event($db, 0);
$event->start = time();
$event->end   = time() + 60*60;

function alertModerators ($event)
{
  global $moderatorlist;
  $mail_title = "Nouvel évènement à modérer : '" . $event->title . "'";
  $mail_body = "Bonjour,\n\n" .
    "Un nouvel évènement est à modérer sur\n" .
    calendar_absolute_url("moderation.php") . "\n\n".
    $event->formatAscii() . "\n\n" . "Merci !\n" .
    "-- ". $GLOBALS["adl_title"];
  $return = true;
  calendar_mail($moderatorlist, $mail_title, $mail_body, $event->id);

  return $return;
}

$_check_ok  = false;
$_sent      = false;
$_preview   = isset($_POST['__event_preview']);

if (isset($_POST['__event_title'])) // if from form
{
  $event->fromForm();
  $event->check();

  $_check_ok = ($event->error==0 || isset($_POST['__event_force']));

  if ($event->message>"")
    error($event->message);

  if ($_check_ok && isset($_POST['__event_save']))
  {
    if (! $event->submitter)
      $event->submitter = $event->contact;

    $event->save();

    if (! $event->error)
    {
      alertModerators($event);
      if (substr($event->submitter,0,4) != "http") $event->notifySubmitter();
      echo "<p><b>Votre évènement a bien été ajouté à la liste des évènements en attente de modération. " .
           "Il apparaîtra en ligne dès qu'un modérateur l'aura validé.</b></p>";
      $_sent = true;
    }
    else
    {
      error($event->message);
    }
    echo "<p><a href=\"index.php\">Retour à l'agenda</a></p>";

  }
  $_preview = true;
}

/* Preview event */
if ($_preview)
{
  echo "<hr/>";
  echo $event->formatHTML();
  echo "<hr/>";
}

if (! $_sent)
{
  echo $adl_submit_summary;
  echo "<div id=\"advises\">\n$adl_submit_advises</div>\n";
  if (isset($_POST['__event_title'])) echo $_POST['__event_title'];

  $event->edit(true, $_check_ok);
}

put_footer();
?>
