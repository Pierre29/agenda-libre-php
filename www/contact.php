<?php

/*
 * Copyright 2012-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/**@file
 *
 * Main page of the Agenda that displays the calendar
 */

include("inc/main.inc.php");

put_header ("Contact");

echo "<h2 style='text-align: center'>Contact</h2>\n";
echo "<p>Pour contacter les modérateurs du site <i>". $GLOBALS["adl_title"]. "</i>&nbsp;:</p>\n";
echo "<ul>\n<li>Par courrier électronique, à".
	"l'adresse <a href='mailto:". $GLOBALS["adl_url_contact"]. "'>". $GLOBALS["adl_url_contact"]. "</a></li>\n";
/*
<li>Par <a href="http://fr.wikipedia.org/wiki/IRC">IRC</a>, sur le
canal <code>#agendadulibre</code> du réseau <i>Freenode</i></li>
*/
echo "</ul>\n";

put_footer();
?>
