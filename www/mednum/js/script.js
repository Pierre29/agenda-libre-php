/*sfHover = function() {
        var sfEls = document.getElementById("menu").getElementsByTagName("LI");
        for (var i=0; i<sfEls.length; i++) {
                sfEls[i].onmouseover=function() {
                        this.className+=" sfhover";
                }
                sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
                }
        }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
*/

function resizeText(multiplier) {
  if (document.body.style.fontSize == "") {
    document.body.style.fontSize = "1.0em";
  }
  document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.1) + "em";
}


var fontSize = 0.8;
function zoom(size)
{
  fontSize = (size == '') ? 0.8 : fontSize + size;
  document.body.style.fontSize = fontSize + "em";
}

// gestion du menu du haut
jQuery.widget("ui.slideDownMenu", {
  _events : {
    "li > a + ul" : {
      "us-slideDownSubMenu-open" : function () {
        var $elf = $(this);
        // Si on est le menu courant on ne fait rien
        if (!$elf.hasClass("current")) {
          // On se $.hide pour mieux s'afficher en utilisant les effet$
          $elf.addClass("current").hide().stop(true, true).css($elf.data("openedCSS"));
          // le menu ainsi pr�par� peut s'afficher.
          $elf.slideDown($elf.data("timer"), "swing");
        } 
      },
      "us-slideDownSubMenu-close" : function () {
        //$(".breadcrumb").html($(".breadcrumb").html()+' close');
        // on n'est plus le menu courant et on stop tout effet
        var $elf = $(this).find('ul');
        $elf.removeClass('current').stop(true, true);
        // On le replie puis on le cache vraiment
        $elf.slideUp();//$elf.data("timer"), $elf.data("slideUpCallback"));
      },
      "us-slideDownMenu-close" : function () {
        // on n'est plus le menu courant et on stop tout effet
        var $elf = $(this).removeClass('current').stop(true, true);
        // On le replie puis on le cache vraiment
        $elf.slideUp($elf.data("timer"), $elf.data("slideUpCallback"));
      },
      "us-slideDownMenu-true-close" : function () {
        // pour vraiment cacher un menu on ne le $.hide pas
        // car la navigation par TAB ne passe pas sur les display:none.
        // et $.hide fait display:none
        var $elf = $(this);
        $elf.css($elf.data("closedCSS")).show(); // au contraire on se $.show
      },
      "us-slideDownMenu-open" : function () {
        var $elf = $(this);
        // Si on est le menu courant on ne fait rien
        if (!$elf.hasClass("current")) {
          // On se $.hide pour mieux s'afficher en utilisant les effet$
          $elf.addClass("current").hide().stop(true, true).css($elf.data("openedCSS"));
          // le menu ainsi pr�par� peut s'afficher.
          $elf.slideDown($elf.data("timer"), "swing");
        }
      },
      "us-init" : function () {
        var $elf = $(this);
        $elf.data("timer", 50 * ($elf.children().length));
        $elf.data("closedCSS", {
            'z-index' : 0,
            "left" : "-9999em"
          }).data("openedCSS", {
            "z-index" : 10,
            "left" : ""
          }).data("slideUpCallback", function () {
            // suite au replie on cache pour de vrai
            $(this).trigger("us-slideDownMenu-true-close");
          }).trigger("us-slideDownMenu-true-close");
      }
    },
    "> ul > li" : {
      "mouseenter" : function () {
        $(this).find("> ul").trigger("us-slideDownMenu-open");
      },
      "mouseleave" : function () {
        var $elf = $(this);
        $elf.find('a.focus').blur();
        $elf.find("> ul").trigger("us-slideDownMenu-close");
      }
    },
    "> ul > li > ul > li" : {
      "mouseenter" : function () {
        $(this).find("> ul").trigger("us-slideDownSubMenu-open");
      },
      "mouseleave" : function () {
        var $elf = $(this);
        $elf.find('a.focus').blur();
        $elf.find("> ul").trigger("us-slideDownSubMenu-close");
      }
    },
    "a" : {
      "focus" : function () {
        $(this).addClass("focus").data("relatedMenu").trigger("us-slideDownMenu-open");
      },
      "blur" : function () {
        var $ul = $(this).removeClass("focus").data("relatedMenu");
        setTimeout(function () {
            if (!$ul.parent().find(".focus").length) {
              // si focus pas pris par un autre <a> du meme menu
              $ul.trigger("us-slideDownMenu-close");
            }
          }, 30);
      },
      "us-init" : function () {
        // data("relatedMenu") est
        // soit le <ul> du menu parent = closest()
        //              (cas des <a> du menu)
        // soit le <ul> du menu enfant = "+ ul"
        //              (cas du <a> de la barre �l�ments DOM fr�re du menu )
        with ($(this))data("relatedMenu", closest("ul").add(find("+ ul")).last());
      }
    }
  },
  /**
   * M�thode attachant de fa�on automatique des �v�nnements
   * � des sous �l�ments du widget en fonction des s�lecteurs.
   **/
  _bindEvents : function () {
    if (this._events) {
      var $uper = this.element;
      $.each(this._events, function (selector, events) {
          $.each(events, function (eventName, eventFunction) {
              $uper.find(selector).add($uper.filter(selector)).bind(eventName, eventFunction);
            });
        });
      this.element.find("*").add(this.element).trigger("us-init");
    }
  },
  /**
   * M�thode d'initialisation du widget
   */
  _create : function () {
    $ = jQuery;
    this.element.addClass("us-slideDownMenu");
    this._bindEvents();
  }
});

jQuery(function($){
$('body').removeClass('no-js');
$('#main-nav').slideDownMenu();

//$("a[rel*=leanModal]").leanModal();

//------------------------------------------------------ SLIDE SHOW
/*
var $carousel = $('#carousel-home');
var $thumbs = $carousel.find('.thumbs'),
$current = {},
currentPage = 1;
if($('#carousel-home').length){ initHome();}

  //variable = 1 si on clique sur les fl�ches
  var clic = 0;
  $thumbs.find('.next').bind('click', function() {
    clic = 1;
  	if(currentPage >= n_page){
  	currentPage = 1;
  	}else{
  	currentPage++;
  	}
  	//alert(currentPage);
  });
    $thumbs.find('.prev').bind('click', function() {
    	clic = 1;
  	if(currentPage <= 1){
  		currentPage = n_page;
  	}else{
  		currentPage--;
  	}
  	//alert(currentPage);
  });

function initHome(){
		$carousel.show();
		// Carousels
		$thumbs.find('a:not(.active) img').fadeTo(200, 0.5);

		$('#carousel-home .large').slides({
			play: 7000,//5000
			pause: 1,//1
			hoverPause: true,
			//fadeSpeed: 700, //effet fusion
			effect: 'slide',
			//crossfade: true,
			animationComplete: showSlide
		});
		$thumbs.slides({
			generatePagination: false,
			generateNextPrev: true
		});
		
		$thumbs.find('a').click(function(){
			var item = parseInt($(this).attr('href').substr(1), 10);
			$thumbs.parent().parent().find('.pagination a').eq(item-1).trigger('click');
			return false;
		});
	}
	
	function showSlide(current){
		//current++ ; // animationStart give the previous id
		//current = (current > $thumbs.find('ul a').length) ? 1 : current;
		var page = Math.ceil(current / 3);
		if(page !== currentPage && clic == 0){
			// force navigation to the good thumb page
			if (page > currentPage) {
				for (var i=0;i<page-currentPage;i++) {
					$thumbs.find('.next').click();
				}
			} else if (page < currentPage) {
				for (var i=0;i<currentPage - page;i++) {
                                        $thumbs.find('.prev').click();
                                }
			}	
			currentPage = page;
			//$thumbs.find('.pagination a').eq(page-1).trigger('click');
		}
		
		$thumbs.find('ul a').removeClass('active');
		$current = $thumbs.find('ul a').eq(current-1);
		$current.addClass('active');
		
		$thumbs.find('ul a').not($current).find('img').fadeTo(2, 0.5);//200,0.5
		$current.find('img').fadeTo(2, 1);//200,1
		
		$thumbs.prev().html($current.attr('title')+'<small>'+ $current.find('img').attr('alt') + '</small>');
		clic = 0;
	}
	//------------------------------------------------------ Widget Formation Animateur
	jQuery(".export_widget_lien").click(function(){
		jQuery(".export_widget").slideToggle();
	});
        /*
	jQuery(".export_widget").click(function(){
		jQuery(".export_widget").hide(500);
		open = 1;
	});*/
});


