/****** BEGIN LICENSE BLOCK *****
 * Copyright (c) 2005-2006 Harmen Christophe and contributors. All rights reserved.
 *
 * This script is free software; you can redistribute it and/or
 *   modify under the terms of the Creative Commons - Attribution-ShareAlike 2.0
 * <http://creativecommons.org/licenses/by-sa/2.0/>
 * You are free:
 *     * to copy, distribute, display, and perform the work
 *     * to make derivative works
 *     * to make commercial use of the work
 * 
 * Under the following conditions:
 * _Attribution_. You must attribute the work in the manner specified by the
 *   author or licensor.
 * _Share Alike_. If you alter, transform, or build upon this work, you may
 *   distribute the resulting work only under a license identical to this one.
 *     * For any reuse or distribution, you must make clear to others 
 *      the license terms of this work.
 *     * Any of these conditions can be waived if you get permission from 
 *      the copyright holder.
 * 
 * Your fair use and other rights are in no way affected by the above.
 * 
 * This is a human-readable summary of the Legal Code (the full license). 
 * <http://creativecommons.org/licenses/by-sa/2.0/legalcode>
 ***** END LICENSE BLOCK ******/

function trim(s) {return s.replace(/(^\s+)|(\s+$)/g,"");}

function hasClassName(oNode,className) {
	return (oNode.nodeType==1)?((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
}

function addClassName(oNode,className) {
	if ((oNode.nodeType==1) && !hasClassName(oNode,className))
		oNode.className = trim(oNode.className+" "+className);
}

function deleteClassName(oNode,className) {
	if (oNode.nodeType==1)
    oNode.className = trim((" "+oNode.className+" ").replace(" "+className+" "," "));
}

function isChildNodeOf(oNode,other) {
	if (oNode.compareDocumentPosition) {
		return (oNode.compareDocumentPosition(other)==10);
	} else if (other.contains) {
		return other.contains(oNode);
	}
	var bIsChildNodeOf = false;
	function _isChildNodeOf(oNode,other) {
		while (other) {
			if (other==oNode) {
				bIsChildNodeOf = true;
				return;
			} else _isChildNodeOf(oNode,other.firstChild);
			other = other.nextSibling;
		}
	}
	_isChildNodeOf(oNode,other.firstChild);
	return bIsChildNodeOf;
}

function addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener](window.event);}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}

function removeEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.removeEventListener) {
		EventTarget.removeEventListener(type,listener, useCapture);
	} else if ((EventTarget==window) && document.removeEventListener) {
		document.removeEventListener(type,listener, useCapture);
	} else if (EventTarget.detachEvent) {
		EventTarget.detachEvent("on"+type, EventTarget[type+listener]);
		EventTarget[type+listener]=null;
		EventTarget["e"+type+listener]=null;
	} else {
		EventTarget["on"+type]=null;
	}
}

/*
	domEl() function - painless DOM manipulation
	written by Pawel Knapik  //  pawel.saikko.com
*/

var domEl = function(e,c,a,p,x) {
if(e||c) {
	c=(typeof c=='string'||(typeof c=='object'&&!c.length))?[c]:c;	
	e=(!e&&c.length==1)?document.createTextNode(c[0]):e;	
	var n = (typeof e=='string')?document.createElement(e) : !(e&&e===c[0])?e.cloneNode(false):e.cloneNode(true);	
	if(e.nodeType!=3) {
		c[0]===e?c[0]='':'';
		for(var i=0,j=c.length;i<j;i++) typeof c[i]=='string'?n.appendChild(document.createTextNode(c[i])):n.appendChild(c[i].cloneNode(true));
		if(a) {for(var i=(a.length-1);i>=0;i--) a[i][0]=='class'?n.className=a[i][1]:n.setAttribute(a[i][0],a[i][1]);}
	}
}
	if(!p)return n;
	p=(typeof p=='object'&&!p.length)?[p]:p;
	for(var i=(p.length-1);i>=0;i--) {
		if(x){while(p[i].firstChild)p[i].removeChild(p[i].firstChild);
			if(!e&&!c&&p[i].parentNode)p[i].parentNode.removeChild(p[i]);}
		if(n) p[i].appendChild(n.cloneNode(true));
	}	
}

/*
Copyright (c) 2006 Stuart Colville
http://muffinresearch.co.uk/archives/2006/04/29/getelementsbyclassname-deluxe-edition/

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.
*/
  
function getElementsByClassName(strClass, strTag, objContElm) {
  strTag = strTag || "*";
  objContElm = objContElm || document;    
  var objColl = (strTag == '*' && document.all) ? document.all : objContElm.getElementsByTagName(strTag);
  var arr = new Array();                              
  var delim = strClass.indexOf('|') != -1  ? '|' : ' ';   
  var arrClass = strClass.split(delim);    
  for (i = 0, j = objColl.length; i < j; i++) {
    if( objColl[i].tagName == undefined ) continue;  
    var arrObjClass = objColl[i].className.split(' ');   
    if (delim == ' ' && arrClass.length > arrObjClass.length) continue;
    var c = 0;
    comparisonLoop:
    for (k = 0, l = arrObjClass.length; k < l; k++) {
      for (m = 0, n = arrClass.length; m < n; m++) {
        if (arrClass[m] == arrObjClass[k]) c++;
        if (( delim == '|' && c == 1) || (delim == ' ' && c == arrClass.length)) {
          arr.push(objColl[i]); 
          break comparisonLoop;
        }
      }
    }
  }
  return arr; 
}

// To cover IE 5 Mac lack of the push method
Array.prototype.push = function(value) {
  this[this.length] = value;                                                              
}

var menu_timerID = null;

function initMenus() {
    var nMenu = document.getElementById("menu");
    var l = 0;
    var ancrea = 'ancrea';
    //nMenu.insertBefore(domEl('p',[domEl('a','Désactiver le menu',[['href','javascript:menuController();']])],[['id','menuControle']]),nMenu.firstChild); 
    var leMenu=nMenu.getElementsByTagName('ul');
    
    meschilds = leMenu[0].childNodes;
    for(var h=0; h<meschilds.length;h++){
    	if(meschilds[h].nodeType == 1){
            var l = l+1;
            if (meschilds[h].childNodes[2] != null) {
              subul = meschilds[h].childNodes[2];
    		      //subul.style.display = "block";
    		      subul.insertBefore(domEl('li',[domEl('a','Aller au menu suivant',[['class','skiplink']])],[['class','skipli']]),subul.firstChild);
              meschilds[h].insertBefore(domEl('a','',[['name','skip'+l],['id','skip'+l],['class','ciblea']]),meschilds[h].firstChild);
            }           
        }
    }
    for(var i=1;i<leMenu.length;i++){
		var liparent;
        liparent=leMenu[i].parentNode;
        addClassName(liparent,"submenu");
        leMenu[i].style.display = "block";
    }
    
    var lesskip = getElementsByClassName('skiplink', 'a', nMenu);
    for(var j=0;j<lesskip.length;j++){
    lesskip[j].href = "#skip"+(j+2);
    if(j+1==lesskip.length){
		lasta = document.createElement("a");
    	lasta.id = "skip"+(j+2);
    	lasta.name = "skip"+(j+2);
        lasta.className= "ciblea";
		nMenu.appendChild(lasta);
        lesskip[j].firstChild.nodeValue="Sortir du menu";
	}
    }
    loadMenus();
}

function loadMenus() {
    var nMenu = document.getElementById("menu");
    if (nMenu.addEventListener) {
		nMenu.addEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.addEventListener("focus",eventLstMontrerMenu,true);
		nMenu.addEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.addEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.addEventListener("blur",eventLstCacherMenus,true);
		nMenu.addEventListener("DOMFocusOut",eventLstCacherMenus,true);

	} else {
		var nA;
		var lessubmenu = getElementsByClassName('submenu', '*', nMenu);
		for (var i=0; i<lessubmenu.length; i++) {
			addEventLst(lessubmenu[i],"mouseover",eventLstMontrerMenu);
			addEventLst(lessubmenu[i],"mouseout",eventLstCacherMenus);
			for (var j=0; nA = lessubmenu[i].getElementsByTagName("a")[j]; j++) {
				addEventLst(nA,"focus",eventLstMontrerMenu);
				addEventLst(nA,"blur",eventLstCacherMenus);
			}
		}
	}
    
    
    /* on ajoute notre image de fond dans les li pour eviter superposition */
    var leMenu=nMenu.getElementsByTagName('ul');
    for(var i=1;i<leMenu.length;i++){
		var cLi;
        cLi=leMenu[i].getElementsByTagName("li");
        for (var j=0; cLi[j]; j++) {
            var refcLi = cLi[j].firstChild;
            nBackground = document.createElement("img");
            nBackground.src = "/wp-content/themes/epn/menu/testImgActives.gif";
            nBackground.className = "itemBackground";
            nBackground.alt="";
            cLi[j].insertBefore(nBackground,refcLi);
        }    
    }
	
    
  //var mCtrl = document.getElementById("menuControle"); 
	//mCtrl.firstChild.firstChild.nodeValue="Désactiver le menu";
	addClassName(nMenu,"withjavascript");
    
}

function unloadMenus() {
	var nMenu = document.getElementById("menu");
    if (nMenu.removeEventListener) {
		nMenu.removeEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.removeEventListener("focus",eventLstMontrerMenu,true);
		nMenu.removeEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.removeEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.removeEventListener("blur",eventLstCacherMenus,true);
		nMenu.removeEventListener("DOMFocusOut",eventLstCacherMenus,true);

	} else {
		var nA;
		var lessubmenu = getElementsByClassName('submenu', '*', nMenu);
		for (var i=0; i<lessubmenu.length; i++) {
			removeEventLst(lessubmenu[i],"mouseover",eventLstMontrerMenu);
			removeEventLst(lessubmenu[i],"mouseout",eventLstCacherMenus);
			for (var j=0; nA = lessubmenu[i].getElementsByTagName("a")[j]; j++) {
				removeEventLst(nA,"focus",eventLstMontrerMenu);
				removeEventLst(nA,"blur",eventLstCacherMenus);
			}
		}
	}
    
    /* on supprime notre image de fond dans les li pour eviter superposition */
    var leMenu=nMenu.getElementsByTagName('ul');
    for(var i=1;i<leMenu.length;i++){
		var cLi;
        cLi=leMenu[i].getElementsByTagName("li");
        for (var j=0; cLi[j]; j++) {
            cLi[j].removeChild(cLi[j].firstChild);
        }    
    }
    
    //var mCtrl = document.getElementById("menuControle");
	  //mCtrl.firstChild.firstChild.nodeValue="Activer le menu";
    deleteClassName(nMenu,"withjavascript");
}

function menuController() {
	var nMenu = document.getElementById("menu");
	if (hasClassName(nMenu,"withjavascript")) unloadMenus();
	else loadMenus();
}

function eventLstMontrerMenu(evt) {
	var oNode;
	if (evt && evt.target) {
		oNode = evt.target;
	} else if (window.event) {
		oNode = window.event.srcElement;
	} else {
		oNode = this;
	}
	if (menu_timerID!=null) {cacherMenus();}
	while (oNode.id!="menu") {
		if (hasClassName(oNode,"submenu")) {
			addClassName(oNode,"encourt");
        } else if(hasClassName(oNode,"skiplink")){
        oNode.parentNode.className = "skiplifocus";
        } else{
            addClassName(oNode,"focus");
        }
		oNode = oNode.parentNode;
	}


	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function eventLstCacherMenus(evt) {
	var oNode, nRelatedTarget;
	if (evt && evt.target) {
		oNode = evt.target;
		nRelatedTarget = evt.relatedTarget;
	} else if (window.event) {
		oNode = window.event.srcElement;
		nRelatedTarget = window.event.toElement;

	} else {
		oNode = this;
	}

	if (nRelatedTarget) {
		var nCacherChildsTheme;
		while (oNode.id!="menu") {
			if ( (hasClassName(oNode,"submenu")) && (isChildNodeOf(nRelatedTarget,oNode)) ) {
				nCacherChildsTheme = oNode;
				break;
			}
			oNode = oNode.parentNode;
		}

		if (nCacherChildsTheme) {
			cacherMenus(nCacherChildsTheme);
        } else {
			menu_timerID = setInterval("cacherMenus()",800);
		}

	} else {
		cacherMenus();
	}

	// Pour MSIE ou il faut annuler le bouillonnement
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}

function cacherMenus(oNode) {
	var nMenu = document.getElementById("menu");
	if (menu_timerID!=null) {
		clearInterval(menu_timerID);
		menu_timerID = null;
	}
	oNode = oNode?oNode:document.getElementById("menu");
	var lessubmenu = getElementsByClassName('submenu','*', nMenu);
    var lessubfocus = getElementsByClassName('focus','*', nMenu);
	var lessubskiplink = getElementsByClassName('skiplink','*', nMenu);
	for (var i=0; lessubmenu[i]; i++) {
		deleteClassName(lessubmenu[i],"encourt");
	}
	for (var j=0; lessubfocus[j]; j++) {
		deleteClassName(lessubfocus[j],"focus");
	}
    for (var k=0; lessubskiplink[k]; k++) {
		lessubskiplink[k].parentNode.className = "skipli";
	}
}

