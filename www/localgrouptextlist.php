<?php

/*
 * Copyright 2008-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");

$localgroup_id = get_safe_integer('id', 0);
if ($localgroup_id>0)
  {
    $localgroup_res = $db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}localgroups WHERE id='{$localgroup_id}'");
    if ($db->numRows($localgroup_res)==0)
    {
      echo "<p class=\"error\">Pas de GUL avec cet identifiant.</p>";
      put_footer();
      exit;
    }
  }
else
  {
    $region_id = get_safe_integer('region', 0);
    $depart_id = get_safe_integer('depart', 0);
    $query = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}localgroups";
    if ($depart_id>0) $query .= " WHERE department='$depart_id'";
    else if ($region_id>0) $query .= " WHERE region='$region_id'";
    $localgroup_res = $db->query ($query);
  }

Header("Content-type: text/plain; charset: utf-8");

echo "lon\tlat\ttitle\tdescription\ticon\n";

while($localgroup = $db->fetchObject($localgroup_res))
{
  // has localgroup coordonates ?
  if ($localgroup->longitude==0)
    { // no
        continue; // no city
    }

  echo
    $localgroup->longitude . "\t" .
    $localgroup->latitude  . "\t" .
    $localgroup->name       . "\t";
  // comment : address + url
  echo
    "<p style=\"font-size: 60%; text-align: center;\"><i>";
  if ($localgroup->address>"")
    echo "{$localgroup->address} - {$localgroup->postalcode}";
  echo " à " . $localgroup->city . "</i><br/>";
  if ($localgroup->comment>"")
    {
      echo "<a href=\"". calendar_absolute_url("showlocalgroup.php?id=". $localgroup->id). "\">".
        calendar_absolute_url("showlocalgroup.php?id=". $localgroup->id). "</a>";
    } else {
      if ($localgroup->url>"")
        {
          echo "<a href=\"". $localgroup->url. "\">". $localgroup->url. "</a>";
        }
    }
  echo "</p>" . "\t" .
    calendar_absolute_url(_themeFile("image/localgroup.png")) . "\n";
}
?>
