<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header("Recommandations sur la modération");
?>

<h2>Recommandations sur la modération</h2>

<p>Tous les évènements de l'Agenda du Libre passent par une phase de
modération, qui permet de s'assurer que les évènements recensés
rentrent bien dans la <a href="#ligne">ligne éditoriale</a> et que la
<a href="#qualite">qualité de la description des évènements</a> reste
bonne.</p>

<a name="ligne"></a>
<h3>Ligne éditoriale</h3>

<p>Tout d'abord, les évènements acceptés sont ceux qui concernent le
Logiciel Libre ou le monde du Libre. Un évènement simplement en
rapport avec les nouvelles technologies n'a pas sa place dans l'Agenda
du Libre.</p>

<p>Ensuite, l'Agenda du Libre a été principalement créé pour recenser
les évènements de la communauté du Logiciel Libre. Les évènements
organisés par les associations, groupes d'utilisateurs, médiathèques,
bibliothèques, lieux d'accès publics à Internet concernant les
Logiciels Libres sont la "cible" principale de l'Agenda.</p>

<p>Les évènements organisés par les entreprises peuvent être acceptés,
à condition&nbsp;:</p>

<ul>

 <li>Que l'accès soit ouvert à tous. Une inscription préalable
 peut-être nécessaire&nbsp;;</li>

 <li>Que l'inscription soit gratuite ou à un tarif
 raisonnable. Certains évènements de la communauté, comme le forum
 PHP, sont également payants, et font partie de la ligne
 éditoriale. Évidemment, le terme <i>raisonnable</i> reste sujet à
 interprétation. Au jour d'aujourd'hui, un évènement dont
 l'inscription coûte 50-100 Euros peut éventuellement être accepté
 dans l'Agenda du Libre. Pour apprécier si un évènement payant doit
 être validé ou non, le thème de celui-ci rentrera en compte&nbsp;:
 une conférence d'interêt général devra être validée, mais pas
 une formation coûteuse ou un évènement de marketing pur pour les
 produits d'une entreprise&nbsp;;</li>

 <li>Que la formulation de la description de l'évènement ne soit pas
 une publicité outrancière, style communiqué de presse, pour la ou les
 entreprises organisatrices.</li>

</ul>

<p>En cas de doute sur le fait que l'évènement soit dans la ligne
éditoriale de l'agenda, ne pas hésiter à en discuter sur la liste des
modérateurs, <code>moderateurs@agendadulibre.org</code>.</p>

<a name="qualite"></a>
<h3>Qualité des descriptions</h3>

<p>D'autre part, une attention particulière doit être portée à
l'orthographe et à la grammaire. Les évènements soumis sont à ce
niveau, comme beaucoup de soumissions sur d'autres sites, de qualité
variable. Il convient donc de corriger au maximum les fautes avant de
valider les évènements. Quelques recommandations&nbsp;:</p>

<ul>

 <li>Le titre de l'évènement ne doit contenir ni la date, ni le lieu,
 et être dans la mesure du possible assez bref&nbsp;;</li>

 <li>Le lien de la ville, qui pointe vers Wikipédia, doit être
 fonctionnel. Il peut être nécessaire de corriger des typos dans le
 nom de la ville (absence d'accents ou de tirets) pour rendre le lien
 fonctionnel&nbsp;;</li>

 <li>La description ne doit pas être dans un style télégraphique, mais
 être rédigée&nbsp;;</li>

 <li>La description doit donner la date, l'heure et le lieu précis de
 l'évènement, une description de celui-ci et le public visé. La
 description de l'évènement doit être la plus compréhensible possible
 pour un néophyte&nbsp;;</li>

 <li>Les tags ne doivent pas contenir le nom de la ville ou des mots
 comme "logiciel" ou "libre", qui ne sont pas des tags pertinents sur
 l'Agenda du Libre. Par contre, ils doivent au moins contenir les noms
 des associations et organisations porteuses de l'évènement, ainsi que
 les logiciels et outils qui seront abordés. On peut également
 préciser le type d'évènement, conférence, atelier, install-party. Par
 exemple <code>toulibre gimp atelier</code> est une bonne liste de
 tags. Les tags doivent être en lettres minuscules, séparés par des
 espaces. Si un tag doit contenir plusieurs mots, il faudra les
 séparer par des tirets. Exemple&nbsp;:
 <code>install-party</code></li>

</ul>

<p>Par ailleurs, le travail de modération ne se limite pas à
accepter ou refuser des évènements et à y corriger des
fautes. Il faut également&nbsp;:</p>

<ul>

 <li>Vérifier la provenance de l'information: vérifier qu'un
 lien donnant plus d'information sur l'évènement est disponible,
 auprès d'une source sûre (site d'un LUG, par exemple)&nbsp;;</li>

 <li>S'assurer que le code HTML est potable&nbsp;;</li>

 <li>Améliorer la description de l'évènement : ajouter un
 lien vers le site du LUG, vers un logiciel ou un projet si
 l'évènement concerne un logiciel ou projet particulier,
 etc.</li>

</ul>

<p>Si des informations manquent, les récupérer sur le site de
l'évènement si elles sont disponibles. Si elles ne le sont pas,
envoyer un courriel au soumetteur de l'évènement en demandant ces
informations, et en suggérant de les ajouter également sur le site
officiel si c'est pertinent. Si la description de l'évènement est
outrageusement incomplète et que l'évènement n'a pas lieu dans les
prochains jours, il est également possible de refuser en utilisant la
raison <i>«&nbsp;pas assez d'informations&nbsp;»</i>.</p>

<h3>Évolution des règles de modération</h3>

<p>Ces recommandations de modération sont à discuter et à
améliorer au fur et à mesure de la vie du site. Nous pouvons en
discuter via la liste <code>moderateurs@agendadulibre.org</code>.</p>

<?php
put_footer();
?>
