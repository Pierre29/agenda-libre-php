<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header ("Informations");

?>

<h2 style="text-align: center">Informations</h2>

<h3>Table des matières</h3>

<ul>
 <li><a href="#about">à propose de ce service</a></li>
 <li><a href="#faq">FAQ</a></li>
</ul>

<a id="about"></a>
<h3>à propos de ce service</h3>

<p>Le projet <a href="https://git.framasoft.org/agenda-libre/agenda-libre-php">agenda libre</a> propose un logiciel libre qui permet de gérer collectivement un agenda.</p>

<p>C'est une solution adapté pour les collectifs ou les organisations qui souhaitent proposer un agenda mutualisé et ouvert aux contributions externes, via un mécanisme de modération.</p>

<p>Ce projet a initialement été écrit pour l'<a href="http://www.agendadulibre.org/">agenda du libre</a>, bien que ce dernier n'utilise dorénavant plus cette base de code.</p>

<p>N'hésitez pas à contribuer au développement de ce logiciel libre sur la <a href="https://git.framasoft.org/agenda-libre/agenda-libre-php">forge de développement</a> de développeurs. Les discussions autour des améliorations à apporter au logiciel ont lieu sur la liste de diffusion devel@agendadulibre.org (cette liste nécessite une <a href="https://listes.agendadulibre.org/wws/subscribe/devel">inscription préalable</a> très simple).</p>

<a id="faq"></a>
<h3>FAQ : questions fréquemment posées</h3>

<ol>

 <li><p><b>Pourquoi vois-je des évènements de Bruxelles ou Lyon
 alors que je suis abonné au flux RSS ou au calendrier iCal de la
 région Midi-Pyrénées ?</b></p>

 <p>À chaque évènement de l'Agenda du Libre est associé une
 <i>portée</i>, <i>locale</i> ou <i>nationale</i>. Si
 l'évènement est local, il apparaît seulement dans le flux
 RSS et le calendrier iCal de la région correspondante. Si
 l'évènement est national, il apparaît dans tous les flux RSS
 et tous les calendriers iCal.</p>

 <p>En effet, il nous a semblé intéressant que certains
 évènements importants: les Rencontres Mondiales du Logiciel
 Libre, le FOSDEM et quelques autres grandes rencontres soient
 visibles dans tous les flux et tous les calendriers.</p>

 <p>Il est donc possible de voir dans vos flux RSS ou calendriers iCal
 des évènements qui ne sont pas de la région
 sélectionnée. Ils sont alors de portée nationale.</p>

 </li>

 <li><p><b>Pourquoi y'a-t-il une étape de modération ? La
 validation pourrait être automatique, non ?</b></p>

 <p>Évidemment, techniquement, la validation d'un évènement
 pourrait être instantanée, dès que l'évènement est
 soumis. En fait, en pratique, cela n'est pour l'instant pas vraiment
 envisageable si l'on souhaite conserver une certaine qualité à
 l'Agenda du Libre. De trop nombreuses soumissions d'évènements
 sont incomplètes (donc incompréhensibles pour le commun des
 mortels), dans un style télégraphique, ou alors contiennent un
 certain nombre de fautes d'orthographe ou de problèmes de mise en
 page. Si l'on souhaite conserver une certaine cohérence dans
 l'Agenda du Libre, alors une modération est vraiment
 nécessaire.</p>

 <p>D'autre part, une modération <i>a posteriori</i> n'est pas possible
 à cause des flux RSS. En effet, dès qu'un évènement est
 validé, il apparaît dans les flux RSS, et est donc chargé
 par tous les aggrégateurs des personnes abonnées au flux en
 question. Il est alors trop tard pour corriger des erreurs, ou
 supprimer cet évènement si nécessaire.</p>

 <p>Aujourd'hui, la modération est réalisée par une équipe
 de 4 personnes, qui pourra être étendue si nécessaire. Si
 l'évènement est dès le départ correct, la modération
 est très souvent réalisée en quelques heures.</p>

 </li>

 <li><p><b>Pourquoi n'y a-t-il que les évènements français ?
 Pourquoi y'a-t-il seulement une case <i>Autre pays</i> ?</b></p>

 <p>L'Agenda du Libre a vocation à faire connaître auprès du
 plus large public les évènements organisés autour du
 Logiciel Libre. Ces évènements n'ont d'intérêt que si ils
 sont accessibles d'un point de vue géographique par les visiteurs
 de l'Agenda du Libre.</p>

 <p>Un Agenda du Libre de toute la francophonie n'a pas vraiment de
 sens: les français ne sont pas susceptibles de se déplacer au
 Québec ou en Afrique francophone pour une install-party ou un
 repas, et vice-versa. Il nous semble donc plus pertinent que des
 initiatives similaires à l'Agenda du Libre se mettent en place
 dans d'autres pays.</p>

 <p>Bien que l'Agenda du Libre soit dédié aux évènements
 français, il est vrai que nous publions parfois des
 évènements de pays limitrophes à la France.</p>

 <p>À noter que d'autres Agenda du Libre ont été lancés pour d'autres
 pays: <a
 href="http://www.agendadulibre.qc.ca">http://www.agendadulibre.qc.ca</a>
 pour le Québec, <a
 href="http://www.agendadulibre.be/">http://www.agendadulibre.be/</a>
 pour la Belgique francophone et <a
 href="http://www.agendadulibre.ch/">http://www.agendadulibre.ch/</a>
 pour la Suisse francophone.</p>

 </li>

 <li><p><b>Puis-je utiliser le logiciel de l'Agenda du Libre pour mon
 agenda ?</b></p>

 <p>Oui, bien sûr, vous le <i>pouvez</i>: le logiciel de l'Agenda du Libre
 est un <i>Logiciel Libre</i>, distribué sous les termes de la
 licence GPL.</p>

 <p>Toutefois, nous ne le <i>conseillons</i> pas: le logiciel faisant
 fonctionner l'Agenda du Libre est très spécifique à cette
 utilisation, et le sera de plus en plus. Nous n'avons absolument pas
 pour objectif de développer un agenda générique. Il existe
 de nombreux logiciels d'agenda de ce type, écrits en PHP,
 disponibles sous licence libre. Vous pouvez par exemple consulter <a
 href="http://directory.fsf.org/productivity/cal/">cette page</a> du
 répertoire de logiciel maintenu par la <a
 href="http://www.fsf.org">Fondation pour le Logiciel Libre</a>.</p>

 </li>

 <li><p><b>Comment faire en sorte que mon groupe d'utilisateurs de
 Logiciels Libres apparaisse dans l'Agenda du Libre ?</b></p>

 <p>Notre liste de groupes d'utilisateurs provient directement de
 celle maintenue par l'AFUL à l'adresse <a
 href="http://aful.org/gul/liste">http://aful.org/gul/liste</a>. Pour
 apparaître sur l'Agenda du Libre, il faut donc demander aux
 mainteneurs de la liste de l'AFUL d'ajouter votre groupe
 d'utilisateurs. Les mainteneurs de l'Agenda du Libre mettent
 périodiquement à jour la liste des GULLs de l'Agenda du Libre depuis
 celle de l'AFUL. N'hésitez pas à les contacter si vous souhaitez une
 mise à jour plus rapide.</p>

 </li>

</ol>

<?php //'
 put_footer();
?>
