<?php

/*
 * Copyright 2004-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");
include("inc/class.export.inc.php");

$eventList = new exportEvent($db);
$eventList->event_id = get_safe_integer('id', 0);
$eventList->region = get_safe_integer('region', 'all');;
$eventList->count = get_safe_integer('count', 10);
$eventList->daylimit = get_safe_integer('daylimit', 30);
$tag = get_safe_string('tag', '');
$eventList->tags = ($tag>'' ? explode(' ', $tag) : array());

$list = $eventList->getEventsList();
if ($list == FALSE)
{
  echo "Erreur lors de la récupération des évènements";
  exit;
}

echo $eventList->icalStartCalendar();
while ($eventList->event->id = $db->getOne($list))
{
    $eventList->event->get();
    echo $eventList->event->toIcal();
}
echo $eventList->icalEndCalendar();

?>
