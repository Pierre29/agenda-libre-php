<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");
include("inc/class.event.inc.php");

$event_id = get_safe_integer('id', 0);
$event = new event($db, $event_id);
if ($event->error || $event_id==0)
{
  put_header("Informations sur un évènement");
  error ($event->message);
  put_footer();
  exit;
}
if (! $event->moderated)
{
  put_header("Informations sur un évènement");
  error (_("L'événement est en attente de modération."));
  put_footer();
  exit;
}

// Select localgroups

$localgroups = $db->query ("select * from {$GLOBALS['db_tablename_prefix']}localgroups ".
    "where department = '". $event->department . "'");

if ($db->numRows ($localgroups)<10 | $event->department<=' ')
  {
    $oRegion = new region($db, $event->region);
    $localgroups = $db->query ("SELECT * FROM {$GLOBALS['db_tablename_prefix']}localgroups ".
        "WHERE department IN (". implode(",", $oRegion->getDepartmentInRegion()). ")" );

    $r = $event->region;
  }


if ($event->latitude>0)
  {
    // start map
    $jcode = "
      <script src=\"http://openlayers.org/api/OpenLayers.js\"></script>
      <script src=\"http://openstreetmap.org/openlayers/OpenStreetMap.js\"></script>
      <script type=\"text/javascript\">
          var lat={$event->latitude}
          var lon={$event->longitude}
          var zoom=11
          var map;
          function init() {
              map = new OpenLayers.Map (\"map\", {
                  controls:[
                      new OpenLayers.Control.Navigation(),
                      new OpenLayers.Control.PanZoomBar(),
                      new OpenLayers.Control.Attribution()],
                      maxResolution: 156543.0399,
                      units: 'm',
              } );

              map.addControl(new OpenLayers.Control.LayerSwitcher());

              layerTilesAtHome = new OpenLayers.Layer.OSM.Mapnik(\"Mapnik\");
              map.addLayer(layerTilesAtHome);

              var lonLat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection(\"EPSG:4326\"), new OpenLayers.Projection(\"EPSG:900913\"));

              map.setCenter (lonLat, zoom);

              var newl = new OpenLayers.Layer.Text('{$adl_localgroups_acronym}', {location: '" .
              calendar_absolute_url("localgrouptextlist.php?". (isset($r) ? "region={$r}" : "depart={$event->department}")) . "'});
              map.addLayer(newl);

              var newl = new OpenLayers.Layer.GeoRSS('AdL', '" . calendar_absolute_url("rss.php?map=1&id={$event_id}") . "');
              map.addLayer(newl);

          }

      </script>\n";
    put_header("Informations sur un évènement", $jcode, "init();");
    // end map
  }
else
  put_header("Informations sur un évènement");

// context div
echo "<div id=\"localgroup-list\">";

// Display campaigns in context div
include_once('inc/class.campaign.inc.php');
$campaigns = new campaigns($db);
// If event in a campaign, display it
echo $campaigns->returnTagMessage($event->tags);

// Display actions in context div
echo "<h1>Actions</h1>\n";
echo "<a href=\"ical.php?id=". $event_id. "\" title=\"Export au format iCal\"><i class='fa fa-calendar'></i> Ajouter à mon calendrier</a><br/>";
if ($session->exists('agenda_libre_id'))
  { // moderateur
    echo "<a href=\"editevent.php?id=" . $event_id . "\"><i class='fa fa-pencil'></i> &Eacute;diter évènement</a><br/>";
    echo "<a href=\"cancelevent.php?id=" . $event_id . "\"><i class='fa fa-trash'></i> Annuler évènement</a>";
  }

// Display Localgroups in context div
if ($db->numRows ($localgroups) > 0)
  {
    echo "<h1>". $adl_localgroups_around. "</h1>";
    echo " <ul>";
    while ($localgroup = $db->fetchObject ($localgroups))
      {
        echo "<li>";
        if ($localgroup->comment>"")
          {
            echo "<a href=\"showlocalgroup.php?id=". $localgroup->id;
            echo "\" title=\"". sprintf(_("Voir la fiche de %s"), $localgroup->name). "\">";
          } else {
            if ($localgroup->url>"")
              {
                echo "<a href=\"". $localgroup->url;
                echo "\" title=\"". sprintf(_("Vers le site de %s"), $localgroup->name). "\">";
              }
          }
        echo $localgroup->name;
        if ($localgroup->comment>"" | $localgroup->url>"")
          {
            echo "</a>";
          }
        echo " (". $localgroup->department. ")</li>";
      }
    echo " </ul>";
  }

echo "</div>\n";

// Display event
echo $event->formatHTML();

// start map
if ($event->latitude>0)
  echo "<div style=\"margin: auto; width:75%; height:400px; border: 1px solid black;\" id=\"map\"></div>\n";
// end map

put_footer();

?>
