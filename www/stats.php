<?php

/*
 * Copyright 2004-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - thanks to David Anderson for the powerful SQL requests
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header("Statistiques");
?>

<h2>Statistiques</h2>

<h3>Statistiques générales</h3>

<?php
$moderated = mysql_result ($db->query ("select count(*) from {$GLOBALS['db_tablename_prefix']}events where moderated=1"), 0);
$unmoderated = mysql_result ($db->query ("select count(*) from {$GLOBALS['db_tablename_prefix']}events where moderated=0"), 0);
?>

<table class="stats">
 <tr class="odd">
  <td class="item">Nombre d'évènements validés depuis la création de l'Agenda</td>
  <td class="value"><?php echo $moderated ?></td>
 </tr>
 <tr class="even">
  <td class="item">Nombre d'évènements en cours de modération</td>
  <td class="value"><?php echo $unmoderated; //'?></td>
 </tr>
</table>

<h3>Statistiques par région</h3>

<?php

$result = $db->query ("select regions.name, count(events.id) as 'event_count' ".
	"from {$GLOBALS['db_tablename_prefix']}regions AS regions left join {$GLOBALS['db_tablename_prefix']}events AS events on regions.id=events.region ".
	"where events.moderated=1 or events.id is null group by regions.name");

echo "<table class=\"stats\">";

$i = 0;
while ($row = $db->fetchRow($result))
{
  if ($i % 2 == 0)
    echo "<tr class=\"odd\">";
  else
    echo "<tr class=\"even\">";

  echo "<td class=\"item\">" . $row[0] . "</td><td class=\"value\">" . $row[1] . "</td>";

  echo "</tr>";

  $i++;
}

echo "</table>";

echo "<h3>Statistiques par commune</h3>";

echo "<p>Seules les villes où plus de trois évènements ont été organisés sont mentionnées.</p>";

$result = $db->query ("select city, count(*) as 'event_count' ".
	"from {$GLOBALS['db_tablename_prefix']}events ".
	"where (moderated=1) group by city having (event_count > 3) ".
	"order by event_count desc");

echo "<table class=\"stats\">";

$i = 0;
while ($row = $db->fetchObject($result))
{
  if ($i % 2 == 0)
    echo "<tr class=\"odd\">";
  else
    echo "<tr class=\"even\">";

  echo "<td class=\"item\">" . $row->city . "</td><td class=\"value\">" . $row->event_count . "</td>";

  echo "</tr>";

  $i++;
}

echo "</table>";

echo "<h3>Statistiques par date</h3>";

$result = $db->query ("SELECT CONCAT(YEAR(start_time), '-', MONTH(start_time)) AS 'month', COUNT(*) AS 'event_count' ".
	"FROM {$GLOBALS['db_tablename_prefix']}events WHERE (moderated=1) GROUP BY EXTRACT(YEAR_MONTH FROM start_time)");

echo "<table class=\"stats\">";

$i = 0;
while ($row = $db->fetchRow($result))
{
  preg_match("~([0-9]{4})-([0-9]{1,2})~", $row[0], $elems);
  $date = strtotime($elems[1]. "-". ($elems[2]). "-01");
  $date = ucfirst(strftime("%B %Y\n", $date));

  if ($i % 2 == 0)
    echo "<tr class=\"odd\">";
  else
    echo "<tr class=\"even\">";

  echo "<td class=\"item\">" . $date . "</td><td class=\"value\">" . $row[1] . "</td>";

  echo "</tr>";
  $i++;
}

echo "</table>";

?>
<?php put_footer(); ?>
