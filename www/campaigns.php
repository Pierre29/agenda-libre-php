<?php

/*
 * Copyright 2015
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");

put_header("Campagnes");

echo "<h2>Campagnes</h2>";

include_once('inc/class.campaign.inc.php');

$campaigns = new campaigns($db);

$op = (isset($_REQUEST['op']) ? $_REQUEST['op'] : null);
$id = get_safe_integer('id', 0);

/*
 *
 * Main page
 *
 */

switch ($op) // compute
{
	
  case "update" :
  {
		$campaign = new campaign($db, $id);
    $campaign->fromForm();
	
		if (! $campaign->check())
		{
			// affichage des erreurs
			echo $campaign->message;
			$op = 'edit2';
		} else { 
			// affichage des avertissements s'il y en a
			echo $campaign->message;
			if ($campaign->is_preview) { // prévisualisation
				echo "<h3>Prévisualisation</h3>";
				echo $campaign->formatHtml();
				$op='edit2';
			} else { // enregistrement
				if ($campaign->save())
				{
					header("Location: campaigns.php?op=saved&id={$campaign->id}");
					exit;
				} else {
					echo $campaign->message;
					$op='edit2';
				}
			}
		}
		break;
  }

	case 'saved' :
	{
		echo "<p>Campagne enregistrée.</p>";
		$op='info';
		break;
	}

  case "delete" :
  {
		$campaign = new campaign($db, $id);
		$campaign->fromConfirmDelFormHtml();
		if ($campaign->error) 
		{
			echo $campaign->message;
			$id = $campaign->id;
			$op='info';
		} else {
			if ($campaign->delete())
			{
				header("Location: campaigns.php?op=deleted");;
				exit;
			} else {
				echo $campaign->message;
				$id= $campaign->id;
				$op='info';
			}
		}
    break;
  }
  
  case "deleted" :
  {
		echo "<p>Campagne supprimée.</p>";
		$op="list";
		break;
	}

} // end switch compute


switch ($op) // display
{

  case "del" :
  {
		echo "<h3>Suppression d'une campagne</h3>";
		$campaign = new campaign($db, $id);
    echo $campaign->confirmDelFormHtml($id);
    break;
  }

  case "new" :
  {
		echo "<h3>Édition d'une nouvelle campagne</h3>";
		$campaign = new campaign($db, 0);
    echo $campaign->formHtml(false);
    break;
  }

  case "dupl" :
  {
		$campaign = new campaign($db, $id);
		echo "<h3>Édition d'après une campagne existante</h3>";
    echo $campaign->formHtml(true);
    break;
  }

  case "edit" :
  {
		$campaign = new campaign($db, $id);
  }
  case "edit2" :
  {
		echo "<h3>Édition</h3>";
    echo $campaign->formHtml(false);
    break;
  }

	case "info" :
	{
		echo $campaigns->formatHtml($id);
		break;
	}

  default : // list campagnes
  {
    echo $campaigns->listHtml();
    break;
  }

} // end switch display

  echo "<p><a href=\"campaigns.php\">".
   _("Liste des campagnes"). "</a> - <a href=\"campaigns.php?op=new\">".
   _("Nouvelle campagne"). "</a></p>\n";

put_footer();

?>
