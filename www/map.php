<?php

/*
 * Copyright 2004-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

$jcode = "
    <script src=\"http://openlayers.org/api/OpenLayers.js\"></script>
    <script src=\"http://openstreetmap.org/openlayers/OpenStreetMap.js\"></script>
    <script type=\"text/javascript\">
        var lat=46.8
        var lon=1
        var zoom=6
        var map;
        function init() {
            map = new OpenLayers.Map (\"map\", {
                controls:[
                    new OpenLayers.Control.Navigation(),
                    new OpenLayers.Control.PanZoomBar(),
                    new OpenLayers.Control.Attribution()],
                    maxResolution: 156543.0399,
                    units: 'm',
            } );

            map.addControl(new OpenLayers.Control.LayerSwitcher());

            layerTilesAtHome = new OpenLayers.Layer.OSM.Mapnik(\"Mapnik\");
            map.addLayer(layerTilesAtHome);

            var lonLat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection(\"EPSG:4326\"), new OpenLayers.Projection(\"EPSG:900913\"));

            map.setCenter (lonLat, zoom);

            var newl = new OpenLayers.Layer.GeoRSS('AdL', '" . calendar_absolute_url("rss.php?region=all&map=1&daylimit=0") . "');
            map.addLayer(newl);

        }

    </script>\n";

put_header("Carte", $jcode, "init();");

echo "<h2>Carte des événements</h2>\n";

echo "<div style=\"margin: auto; width:70%; height:600px; border: 1px solid black;\" id=\"map\"></div>\n";

put_footer();
?>
