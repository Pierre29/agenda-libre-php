<?php

/*
 * Copyright 2007-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include("inc/main.inc.php");

put_header("Mots-clés");

echo "<h2>Mots-clés</h2>";

$db = new db();

// Build query to get tags and count of events
$query = "SELECT tags.name as tag, COUNT(events.id) as nb FROM {$GLOBALS['db_tablename_prefix']}events AS events".
  " LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags_events ON event_id=events.id".
  " LEFT JOIN {$GLOBALS['db_tablename_prefix']}tags AS tags ON tags.id=tag_id".
  " WHERE events.moderated=1".
  (! isset($_REQUEST['all']) ? " AND TO_DAYS(". $db->quote_smart($now).
    ") - TO_DAYS(events.start_time) <= 90 " : "").
  " GROUP BY tags.id ORDER BY tags.name";

if (! $result = $db->query($query))
{
  error ("<p>Erreur lors de la requête SQL.</p>");
  put_footer();
  exit;
}

// get max count of events
$maxCountEvent=0;
while ($record = $db->fetchObject($result))
{
  if ($maxCountEvent < $record->nb) $maxCountEvent = $record->nb;
}
$db->freeResult($result);

$maxCountEvent = log($maxCountEvent);

// max font size
$maxFontSize = 300;

// Replay query
$result = $db->query($query);
$countTags = $db->numRows($result);

echo "<p style=\"text-align: center; margin-top: 40px; line-height: 50px;\">";
while ($record = $db->fetchObject($result))
{
  $count = $record->nb;
  $tag   = $record->tag;
  // only tags used thice or more
  if ($tag=="" || ($count<2 && $countTags>20)) continue;

  $size = round(log($count)/$maxCountEvent*$maxFontSize, 2);
  if ($size>80) echo "<span style=\"font-size:{$size}%;\">";
  echo "<a href=\"listevents.php?tag=" . $tag. "\">";
  echo $tag;
  echo "</a>";
  /*echo "<sub style=\"font-size: 10px;\">";
  echo "<a href=\"rss.php?tag=" . $tag . "\">rss</a>/";
  echo "<a href=\"ical.php?tag=" . $tag ."\">ical</a>";
  echo "</sub> ";*/
  if ($size>80) echo "</span>\n";
  echo "&nbsp; &nbsp;";
} // end while
echo "</p>";

$db->freeResult($result);

if (! isset($_REQUEST['all'])) {
    echo "<p>Seuls les mots-clés portant sur plus d'un évènement de moins de 3 mois".
        " ou à venir sont affichés dans cette liste.</p>\n";
    echo "<p>Pour avoir le nuage des mots clés pour tous les événements de l'agenda,".
        " <a href='tags.php?all'>suivez ce lien</a>.</p>\n";
}
else
{
    echo "<p>Pour ne voir le nuage que des mots-clés portant sur plus ".
        " d'un évènement de moins de 3 mois ou à venir ".
        " <a href='tags.php'>suivez ce lien</a>.</p>\n";
}

put_footer();

?>
