<?php

/*
 * Copyright 2005-2015
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Perform a SQL query
 *
 * @param[in] The SQL query to be performed
 */

class db
{
  function db()
  {
    global $db_host;
    global $db_user;
    global $db_pass;
    global $db_name;

    if(@mysql_connect($db_host,$db_user,$db_pass)==FALSE)
      {
          echo "$db_host $db_user $db_pass";
	      echo "Probleme de connexion à la base de données sur $db_host.\n";
	      return 0;
      }

    if(@mysql_select_db($db_name) == FALSE)
      {
	      echo "Problème de selection de la base de données $db_name sur $db_host.\n";
	      return 0;
      }
      
    mysql_query("set names 'utf8'");
  }

  function query ($query)
  {
    if( ($result = @mysql_query($query)) == FALSE)
      {
		echo "Probleme dans la syntaxe de $query : " . mysql_error() . "\n";
		return 0;
      }

    return $result;
  }

  function insertid ()
  {
    return mysql_insert_id ();
  }

  function fetchObject ($result)
  {
    return mysql_fetch_object($result);
  }

  function fetchArray ($result)
  {
    return mysql_fetch_array($result);
  }

  function fetchRow ($result)
  {
    return mysql_fetch_row($result);
  }

  function freeResult ($result)
  {
    return mysql_free_result($result);
  }

  function numRows ($result)
  {
    return mysql_num_rows($result);
  }
  
  function getOne($result) {
	$row = $this->fetchArray($result);
	return $row[0];
  }

  /**
   * Converts the argument of an SQL request in a format accepted by MySQL.
   *
   * @param[in] value String or integer to use as argument
   *
   * @return The string to use in the request
   */
  function quote_smart($value)
  {
    if (get_magic_quotes_gpc())
      $value = stripslashes($value);

    if (!is_numeric($value))
      $value = "'" . mysql_real_escape_string($value) . "'";

    return $value;
  }
} // end class
