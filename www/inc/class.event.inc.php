<?php

/*
 * Copyright 2004-2016
 * - Melanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once('inc/class.geocode.inc.php');
include_once('inc/class.region.inc.php');
include_once('inc/class.tagsHtml.inc.php');

class event {
  var $id;
  var $title,
      $description,
      $start_time, $start,
      $end_time, $end,
      $geocode,         // class geocode
      $place,           // text input by user
      $osmId,           // place id from geocode
      $address,
      $postalcode,
      $city,
      $department,
      $region,
      $country,
      $latitude,
      $longitude,
      $locality,        // portee : -1=locale 0=regionale 1=nationale 2=internationale
      $url,
      $contact,
      $submitter,
      $moderated,
      $secret,
      $decision_time,
      $submission_time;
  var $tags;
  var $error, $message;
  var $db;

  // -------------------------------------------------------------------

  function event($db, $id=0)
  {
    $this->db = $db;
    $this->error = 0;
    $this->message = "";
    $this->geocode = new geocode($db);
    if ($id>0)
    {
      $this->id = $id;
      $this->get();
    }
    return ! $this->error;
  }

  // -------------------------------------------------------------------

  function get()
  {
    $query = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}events WHERE id=" . $this->db->quote_smart($this->id);
    $result = $this->db->query ($query);
    if (! ($record = $this->db->fetchObject($result)))
    {
      $this->message = "Aucun évènement avec l'ID ". $this->id;
      $this->error = true;
      return false;
    }
    $this->title            = $record->title;
    $this->description      = $record->description;
    $this->start_time       = $record->start_time;
    $this->end_time         = $record->end_time;
    $this->start            = strtotime($this->start_time);
    $this->end              = strtotime($this->end_time);
    $this->address          = $record->address;
    $this->place            = $this->address;
    $this->postalcode       = $record->postalcode;
    $this->city             = $record->city;
    $this->department       = $record->department;
    if ($this->department>0)
    {
        $region = new region($this->db);
        $this->region       = $region->getRegionFromDepartment($record->department);
        unset($region);
    } else {
        $this->region       = $record->region;
    }
    //$this->country           = $record->country;
    $this->latitude         = $record->latitude;
    $this->longitude        = $record->longitude;
    $this->locality         = $record->locality;
    $this->url              = $record->url;
    $this->contact          = $record->contact;
    $this->submitter        = $record->submitter;
    $this->moderated        = $record->moderated;
    $this->secret           = $record->secret;
    $this->decision_time    = $record->decision_time;
    $this->submission_time  = $record->submission_time;
    $oTags = new tagsHtml($this->db);
    $this->tags             = $oTags->eventTagsList($this->id);

    $this->message = "";
    $this->error = false;
    return true;
  }

  // -------------------------------------------------------------------

  function save()
  {
    global $now;
    $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
    $sql .= "{$GLOBALS['db_tablename_prefix']}events SET " .
      "title=" .       $this->db->quote_smart ($this->title)                        . ", " .
      "start_time=" .  $this->db->quote_smart (date_timestamp2mysql ($this->start)) . ", ".
      "end_time=" .    $this->db->quote_smart (date_timestamp2mysql ($this->end))   . ", ".
      "description=" . $this->db->quote_smart ($this->description)                  . ", ".
      "address=" .     $this->db->quote_smart ($this->address)                      . ", ".
      "department=" .  $this->db->quote_smart (floor($this->postalcode/1000))       . ", ".
      "postalcode=" .  $this->db->quote_smart ($this->postalcode)                   . ", ".
      "city=" .        $this->db->quote_smart ($this->city)                         . ", ".
      "region=" .      $this->db->quote_smart ($this->region)                       . ", ".
      //"country=" .     $this->db->quote_smart ($this->country)                      . ", ".
      "latitude=" .    $this->db->quote_smart ($this->latitude)                     . "," .
      "longitude=" .   $this->db->quote_smart ($this->longitude)                    . "," .
      "locality=" .    $this->db->quote_smart ($this->locality)                     . ", ".
      "url=" .         $this->db->quote_smart ($this->url)                          . ", ".
      "contact=" .     $this->db->quote_smart ($this->contact)                      . ", ".
      "submitter=" .   $this->db->quote_smart ($this->submitter);
    if ($this->id==0)
      $sql .= ", moderated='0', ".
        "secret='" . ($this->secret = md5(uniqid(rand(), true))) . "', ".
        "submission_time=". $this->db->quote_smart($now);
    else
      $sql .= " WHERE id=" .    $this->db->quote_smart ($this->id);

    $ret = $this->db->query($sql);
    if ($ret == FALSE)
    {
      $this->error=true;
      $this->message = "La requête <i>" . $sql . "</i> a échoué";
      return false;
    }
    // about tags
    if ($this->id==0) $this->id = $this->db->insertid();
    else
    {
      // delete old
      $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events WHERE event_id='{$this->id}'";
      $result = $this->db->query($query);
      if (! $result)
      {
        $this->error=true;
        $this->message=_("Impossible de supprimer les anciens mots-clés");
        return false;
      }
    }
    // add new
    // manual tags
    $aTags = explode(" ", $this->tags);
    $aTagsKnown = array();
    // get tags which already exist
    $query = "SELECT id, name FROM {$GLOBALS['db_tablename_prefix']}tags WHERE name IN ('". implode("', '", $aTags). "')";
    $result = $this->db->query($query);
    if (! $result)
    {
      $this->error=true;
      $this->message=_("Impossible de lire les mots-clés");
      return false;
    }
    while ($record = $this->db->fetchObject($result))
    {
      $aTagsKnown[]=$record->name;
    }
    // Insert new tags
    if (count($aTagsNew = array_diff($aTags, $aTagsKnown))>0)
    {
      $query = "INSERT INTO {$GLOBALS['db_tablename_prefix']}tags (name) ".
        "VALUES ('". implode("'), ('", $aTagsNew). "')";
      $result = $this->db->query($query);
      if (! $result)
      {
        $this->error=true;
        $this->message=_("Impossible d'ajouter les mots-clés");
        return false;
      }
    }
    // make link between event and tags
    $query = "INSERT INTO {$GLOBALS['db_tablename_prefix']}tags_events (event_id, tag_id) ".
      "SELECT {$this->id}, id FROM {$GLOBALS['db_tablename_prefix']}tags ".
      "WHERE name IN ('". implode("','", $aTags). "')";
    $result = $this->db->query($query);
    if (! $result)
    {
      $this->error=true;
      $this->message=_("Impossible d'attribuer les mots-clés");
    }
    return ! $this->error;
  } // end function_save();

  // -------------------------------------------------------------------

  function delete()
  {
    $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}events WHERE id=" . $this->db->quote_smart($this->id);
    $ret = $this->db->query($sql);
    if (! $ret)
    {
      $this->error=true;
      $this->message = returnError("Impossible de supprimer l'événement");
      error ("La requête <i>" . $sql . "</i> a échoué");
      return false;
    }
    // delete link between event and tags
    $query = "DELETE FROM {$GLOBALS['db_tablename_prefix']}tags_events ".
      "WHERE event_id=". $this->db->quote_smart($this->id);
    $ret = $this->db->query($query);
    if (! $ret)
    {
      error("Impossible de supprimer les mots-clés");
      // not required to return true
    }
    // else
    // clear tags table of tags not used
    return true;
  }

  // -------------------------------------------------------------------

  function cancel()
  {
    global $now;

    $sql = "UPDATE {$GLOBALS['db_tablename_prefix']}events SET " .
      " moderated='0', decision_time=". $this->db->quote_smart($now).
      " WHERE id=" .    $this->db->quote_smart ($this->id);
    $ret = $this->db->query($sql);
    if ($ret == FALSE)
    {
      $this->error=true;
      $this->message = "La requête <i>" . $sql . "</i> a échoué";
      return false;
    }
    return ! $this->error;
  } // end cancel();

  // -------------------------------------------------------------------

  function edit($previewEnable=false, $submitEnable=true, $postUrl="submit.php")
  {
    global $adl_form_description_guide, $adl_form_tags_guide;

    echo "<form method=\"post\" name='event' action=\"{$postUrl}\">\n";

    if (file_exists("tiny_mce"))
    {
      ?>
  <!-- tinyMCE -->
  <script language="javascript" type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
  <script language="javascript" type="text/javascript">
          // Notice: The simple theme does not use all options some of them are limited to the advanced theme
          tinyMCE.init({
                  mode : "textareas",
                  language : "fr",
                  theme : "advanced",
                  plugins : "paste,preview",
                  theme_advanced_buttons1 : "bold,italic,underline,separator, bullist,numlist,undo,redo,link,unlink,separator,cut,copy,paste,pastetext,pasteword,separator,preview,code",
                  theme_advanced_buttons2 : "",
                  theme_advanced_buttons3 : "",
                  theme_advanced_toolbar_location : "top",
                  theme_advanced_toolbar_align : "left",
                  theme_advanced_path_location : "bottom",
                  plugin_preview_width : "500",
                  plugin_preview_height : "600",
                  extended_valid_elements : "a[name|href|target|title|onclick]"
          });
  </script>
  <!-- /tinyMCE -->
  <?php
    } // end if tiny_mce

  ?>
   <table class='form'>

    <tr><td>Titre*:</td>
     <td><b>Décrivez en moins de 5 mots votre évènement, sans y indiquer le lieu, la ville ni la date.</b><br/>
      <input type="text" size="70" name="__event_title" value="<?php echo $this->title;?>"/></td>
    </tr>

    <tr><td>Début*:</td>
     <td><?php $this->generate_date_forms("start", $this->start); ?></td>
    </tr>

    <tr><td>Fin*:<br/><br/></td>
     <td><?php $this->generate_date_forms("end", $this->end); ?><br/></td>
    </tr>

    <tr><td>Description*:</td>
     <td><b>Décrivez de la manière la plus complète possible votre évènement.</b><br/>
      <?php
        if (file_exists("tiny_mce"))
          echo "<noscript>". $adl_form_description_guide. "</noscript>\n";
        else
          echo $adl_form_description_guide. "\n";
      ?>
      <textarea rows="25" cols="70" name="__event_description"><?php echo $this->description;?></textarea></td>
    </tr>

    <tr><td>Adresse*:</td>
     <td>
      <?php
        if (count($this->geocode->addresses)>1)
        {
            echo "<fieldset><legend><b>{$this->geocode->message}</b></legend>\n";
            echo $this->geocode->found2input('__event_osmId');
            echo "ou alors saisir de nouveau<br/><label for='new'><input type='text' size='70' name='__event_place' ".
                "value=\"{$this->place}\" ".
                "onfocus=\"document.forms.event.__event_osmId[document.forms.event.__event_osmId.length - 1].checked=true;\"/>".
                "<input id='new' type='radio' name='__event_osmId' value='new'></label>\n";
            echo "</fieldset>\n";
        } elseif (count($this->geocode->addresses)==1) {
            echo "<i>Adresse postale reconnue.</i><br/>\n";
            echo "<input type='text' size='70' name='__event_place' value=\"{$this->place}\" ".
                " onchange=\"document.forms.event.__event_osmId.value='new';\" />";
            echo "<input type='hidden' name='__event_osmId' value=\"{$this->osmId}\"/>";
        } else {
            echo "<i>Adresse postale seule pour la géolocalisation.</i><br/>\n";
            echo "<input type='text' size='70' name='__event_place' value=\"{$this->place}\"/>";
        }
      ?>
     </td>
    </tr>

    <tr><td>Portée*:</td>
     <td><select name="__event_locality"><?php
          foreach ($GLOBALS["adl_locality"] as $key=>$value)
            echo "<option value=\"$key\"". ($this->locality == $key ? " selected=\"selected\"" : ""). ">$value</option>\n";
        ?></select></td>
    </tr>

    <tr><td>URL*:</td>
     <td><i>Lien <b>direct</b> vers une page donnant plus d'informations sur
      l'évènement (lieu et horaire précis, programme détaillé...) au format <code>https://url</code>
      ou <code>http://url</code> </i><br/>
      <input type="text" size="70" name="__event_url" value="<?php echo $this->url;?>"/></td>
    </tr>

    <tr><td>Contact*:</td>
     <td><i>Adresse courriel de contact (elle sera transformée de manière à éviter le spam) <b>ou</b>
      Lien direct vers un formulaire de contact au format <code>https://url</code> ou <code>http://url</code></i><br/>
      <input type="text" size="70" name="__event_contact" value="<?php echo $this->contact;?>"/></td>
    </tr>

    <tr><td>Soumetteur:</td>
     <td><i>Adresse courriel du soumetteur de l'évènement (qui ne sera utilisée que par les modérateurs pour informer de la validation ou du rejet). A défaut, l'adresse de contact sera utilisée.</i><br/>
      <input type="text" size="70" name="__event_submitter" value="<?php echo $this->submitter;?>"/></td>
    </tr>
  <?php
    $aCategoryTags = new tagsHtml($this->db);
    $aCategoryTags->selectWithCategoryTags(explode(" ", $this->tags));
    foreach ($aCategoryTags->HTML as $category_id=>$aSelect)
      echo "<tr><td>{$aSelect['categoryName']}:</td>".
        "<td>{$aSelect['categoryDescription']}<br />".
        "{$aSelect['HTMLSelect']} </td>\n</tr>\n";
    $aTags = explode(' ', $aCategoryTags->tagsRest);
  ?>
    <tr><td>Mots-clés :</td>
     <td><?php
    echo $adl_form_tags_guide;
    $oTags = new tagsHtml($this->db);
    $oTags->selectCategoryTags($aTags, 0, $fieldName="tags[0]", $multiple=true);

    // campagne en cours, on présente les tags associés
    include_once('inc/class.campaign.inc.php');
    $campaigns = new campaigns($this->db);
    echo ($campaignInProgress = $campaigns->returnTagExplain());

    echo _("Dans les mots-clés courants"). " : <br />". $oTags->HTML;
    $tags = $oTags->tagsRest;
    ?><br /><input type="text" size="70" name="__event_tags" value="<?php echo $tags; ?>"/></td>
    </tr>

   <tr><td></td>
    <td><?php
    if ($previewEnable)
      echo "<input name=\"__event_preview\" type=\"submit\" value=\"Aperçu\"/>";
    echo "<input name=\"__event_save\" type=\"submit\" value=\"Valider\" ".
      (! $submitEnable ? "disabled='disabled'" : ""). "/>";
  ?></td>
   </tr>

   </table>
  </form>
  <?php
  } // end function_edit

  // -------------------------------------------------------------------

  /**
   * Format an event in an ASCII format, suitable for e-mails
   *
   * @return An ASCII description of the event
   */
  function formatAscii()
  {
    $title       = stripslashes($this->title);
    $start       = date_timestamp2humanreadable($this->start);
    $end         = date_timestamp2humanreadable($this->end);
    $address     = stripslashes($this->address);
    $postalcode  = stripslashes($this->postalcode);
    $city        = stripslashes($this->city);
    $oRegion = new region($this->db, $this->region);
    $region      = stripslashes($oRegion->name);
    $description = html_entity_decode(stripslashes($this->description));
    $url         = stripslashes($this->url);
    $contact     =  (strncmp ($this->contact, "http://", 7) && strncmp($this->contact, "https://", 8)) ?
            scramble_email(stripslashes($this->contact)) : stripslashes($this->contact);
    $submitter   = scramble_email(stripslashes($this->submitter));
    $tags        = stripslashes($this->tags);

    $str =
      "Titre       : " . $title . "\n" .
      "Début       : " . $start . "\n".
      "Fin         : " . $end . "\n" .
      "Adresse     : " . $address . "\n" .
      "Code postal : " . $postalcode . "\n" .
      "Ville       : " . $city . "\n" .
      "Région      : " . $region . "\n" .
      "URL         : " . $url . "\n".
      "Contact     : " . $contact . "\n" .
      "Soumetteur  : " . $submitter . "\n" .
      "Mots-clés   : " . $tags . "\n" .
      "Description : \n " . wordwrap(preg_replace ("/\n/", "\n ", strip_tags(utf8_encode($description))));

    return $str;
  }

  // -------------------------------------------------------------------

  /**
   * Format an event so that it can be displayed in a nice fashion
   *
   * @param db Database handle
   *
   * @param title Title of the event
   *
   * @param start Starting time (timestamp format)
   *
   * @param end Ending time (timestamp format)
   *
   *
   * @return The HTML code that corresponds to the formatted event
   */
  function formatHtml($moderation = FALSE)
  {
    $title       = stripslashes($this->title);
    //$start       = date_timestamp2humanreadable($this->start);
    $end         = date_timestamp2humanreadable($this->end);
    $address     = stripslashes($this->address);
    $postalcode  = stripslashes($this->postalcode);
    $city        = stripslashes($this->city);
    $regionO = new region($this->db, $this->region);
    $region      = stripslashes($regionO->name);
    //$description = stripslashes($this->description);
    $url         = stripslashes($this->url);
    if (strncmp ($this->contact, "http://", 7) && strncmp($this->contact, "https://", 8))
    {
        $contact = "mailto:". scramble_email(stripslashes($this->contact));
    } else {
        $contact = stripslashes($this->contact);
    }
    $start_day = onlyday_timestamp2humanreadable($this->start);
    if ($start_day == onlyday_timestamp2humanreadable($this->end)) {
      $date = "<p>Le " .  $start_day . ", de "
        . onlyhour_timestamp2humanreadable($this->start) . " à "
        . onlyhour_timestamp2humanreadable($this->end) . ".</p>\n";
    } else {
      $date = "<p>Du " . date_timestamp2humanreadable($this->start)
        . " au " . date_timestamp2humanreadable($this->end) . ".</p>\n";
    }

    $result  = "<h2><i>" . $city . "</i> : " . $title . "</h2>\n\n";
    $result .= "<h3>Date et lieu</h3>\n";
    $result .= $date;

    $result .= "<p>à $address - $postalcode <i><a href=\"http://fr.wikipedia.org/wiki/" . urlencode(str_replace(' ', '_', $city)) . "\">"
              . $city . "</a></i>, <a href=\"http://fr.wikipedia.org/wiki/"
              . $region . "\">" . $region . "</a></p>\n\n";
    $result .= "<h3>Description</h3>\n";
    $result .= $this->description . "\n\n";
    $result .= "<h3>Informations</h3>\n";
    $result .= "<p>Site Web: <a href=\"" . $url . "\">" . $url . "</a></p>\n";
    $result .= "<p>Contact: <a href=\"" . $contact . "\">" . $contact . "</a></p>\n";

    if ($moderation)
      $result .= "<p>Évènement à portée <b>". $GLOBALS["adl_locality"][$this->locality]. "</b></p>";

    if ($this->tags != "")
    {
        $oTags = new tagsHtml($this->db);
        $result .= $oTags->formatTagsHtml($this->tags);
    }

    return $result;
  }

  // -------------------------------------------------------------------

  /*
   * Came from From (edit function)
   *
   */
  function fromForm()
  {
    /* Convert form date to timestamp */
    if (isset($_POST['__event_start_day']))
    {
      $this->start = strtotime(
        $_POST['__event_start_year']. "-". $_POST['__event_start_month'].
        "-". $_POST['__event_start_day']. " ".
        $_POST['__event_start_hour']. ":". $_POST['__event_start_minute']. ":00"
      );
    }

    if (isset($_POST['__event_end_day']))
    {
      $this->end = strtotime(
        $_POST['__event_end_year']. "-". $_POST['__event_end_month'].
        "-". $_POST['__event_end_day']. " ".
        $_POST['__event_end_hour']. ":". $_POST['__event_end_minute']. ":00"
      );
    }

    $afieldName = array('title', 'description',
      'place', 'osmId', //'address', 'postalcode', 'city', 'region', 'locality',
      'url', 'contact', 'submitter');

    foreach ($afieldName as $fieldName)
    {
      if (isset($_POST['__event_'. $fieldName]))
        $this->$fieldName = trim(stripslashes(strip_tags($_POST['__event_'. $fieldName],
          $fieldName=='description' ? "<p><strong><em><br/><a><ul><li><ol><b><i>" : "")));
    }

    if (isset($_POST['__event_tags']))
    { // format tags
      $event_tags = $_POST['__event_tags'];
      if (isset($_POST['tags']) && is_array($_POST['tags']))
      {
        foreach($_POST['tags'] as $aTags)
          if (is_array($aTags))
            $event_tags .= " ". implode(' ', $aTags);
          else
            $event_tags .= " ". $aTags;
      }
      $this->tags = stripslashes(strip_tags(trim(str_replace("  ", " ", $event_tags))));
    }
    else
      $this->tags = "";

    return true;

  } // end function_fromForm

  // -------------------------------------------------------------------

  // Check information from event edit form
  function check()
  {
  global $session, $tagMaxSize, $tagMinSize;
    $error_cnt = 0;

    if (! $this->title)
      {
        $this->message .= returnError("Titre vide");
        $error_cnt++;
      }

    if (stristr($this->title, "viagra"))
      {
        $this->message .= returnError ("Les spammeurs, dehors");
        $error_cnt++;
      }

    if (! $this->description)
      {
        $this->message .= returnError ("Description vide");
        $error_cnt++;
      }

    if (! $this->place)
      {
        $this->message .= returnError ("Adresse vide");
        $error_cnt++;
      }
    elseif ($this->osmId && $this->osmId!='new')
      { // the user selected a place from multiple
        $this->geocode->findFromOsmId($this->osmId);
        if ($this->geocode->error) {
            $this->message = returnWarning($this->geocode->message);
        }
        else
        {
            $this->geocode->toEvent($this);
        }
      }
    else
      { // we have an address, improve it
        $this->geocode->place = $this->place;
        $this->geocode->findFromPlace();

        if ($this->geocode->error) // error
          {
            $this->message .= returnError($this->geocode->message);
            $error_cnt++;
          }
        elseif (count($this->geocode->addresses)==1)
          { // only one address found
            $this->geocode->toEvent($this);
          }
        else
          { // some addresses found
            $this->message .= returnError($this->geocode->message);
            $error_cnt++;
          }
      }

    if (! $this->url)
      {
        $this->message .= returnError ("URL vide");
        $error_cnt++;
      }
    elseif (strncmp ($this->url, "http://", 7) && strncmp($this->url, "https://", 8))
      {
        $this->message .= returnError ("URL ne commençant pas par http:// ou https://");
        $error_cnt++;
      }

    if ($this->start <= time())
      {
        $mes = "Le début de l'évènement est dans le passé.";
        if (! isset($session) or ! $session->exists('agenda_libre_id'))
        {
            $this->message .= returnError($mes);
            $error_cnt++;
        } else {
            $this->message .= returnWarning($mes);
        }
      }

    if ($this->end <= time())
      {
        $mes = "La fin de l'évènement est dans le passé.";
        if (! isset($session) or ! $session->exists('agenda_libre_id'))
        {
            $this->message .= returnError($mes);
            $error_cnt++;
        } else {
            $this->message .= returnWarning($mes);
        }
      }

    if ($this->end <= $this->start)
      {
        $this->message .= returnError ("La fin de l'évènement est avant le début");
        $error_cnt++;
      }

    if (! preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~", $this->contact))
      {
        if (strncmp ($this->contact, "http://", 7) && strncmp($this->contact, "https://", 8))
            {
                $this->message .= returnError ("Contact, ni courriel valide, ni URL valide commençant pas par http:// ou https://");
                $error_cnt++;
            }
      }

    if (!empty($this->submitter) && !
      preg_match("~^([-A-Za-z0-9_+.]*)@([-A-Za-z0-9_]*)\.([-A-Za-z0-9_.]*)$~",
      $this->submitter))
      {
        $this->message .= returnError ("Courriel du soumetteur invalide");
        $error_cnt++;
      }

    $tags = str_replace(" +", " ", $this->tags);
    foreach(explode(" ", $tags) as $tag)
      {
        if ($tag == "")
          continue;

        if (! preg_match("~^[a-z0-9\-]*$~", $tag))
          {
            $this->message .= returnError("Tag '" . $tag . "' invalide. ".
              "Les tags ne doivent contenir que des lettres minuscules ".
              "sans accent, des chiffres ou des tirets.");
            $error_cnt++;
          }
        if (strlen($tag) < $tagMinSize || strlen($tag) > $tagMaxSize)
          {
            $this->message .= returnError(sprintf("Tag '" . $tag . "' trop court ou trop long. ".
                "La longueur des tags doit être de %s à %s caractères.", $tagMinSize, $tagMaxSize));
            $error_cnt++;
          }
      }

    $this->error = $error_cnt;
    return ($error_cnt == 0);
  } //

  // -------------------------------------------------------------------

  function notifySubmitter ()
  {
    $mail_title = "Votre évènement : '" . $this->title . "' est en attente de modération";

    $mail_body = "Bonjour,\n\n" .
      wordwrap("Votre évènement intitulé '" . $this->title .
               "', qui aura lieu le '" . date_timestamp2humanreadable($this->start) .
               "' a bien été enregistré dans ". $GLOBALS['adl_title']. ". ".
               "L'équipe de modération le prendra en charge très prochainement. " .
               "Pendant la modération et après celle-ci si votre évènement est validé, " .
               "vous pouvez éditer votre évènement à l'adresse :\n" .
               "  " . calendar_absolute_url("editevent.php?id=" .
                $this->id . "&secret=" . $this->secret) . "\n\n" .
               "et vous pouvez l'annuler en utilisant l'adresse :\n" .
               "  " . calendar_absolute_url("cancelevent.php?id=" .
                $this->id . "&secret=" . $this->secret) . "\n\n") .
      "Merci de votre participation !\n" .
      "-- ". $GLOBALS["adl_title"];

    calendar_mail($this->submitter, $mail_title, $mail_body, $this->id);

    return true;
  }

  // -------------------------------------------------------------------

  function toIcal()
  {
    global $timezone, $adl_uid_end;
    $this->url = calendar_absolute_url("showevent.php?id=" . $this->id);
    $ret = "BEGIN:VEVENT\r\n".
      "DTSTART;TZID={$timezone}:" .  date ('Ymd\THi\0\0', $this->start) . "\r\n".
      "DTEND;TZID={$timezone}:"   .  date ('Ymd\THi\0\0', $this->end) . "\r\n".
      "UID:" . $this->id . "@$adl_uid_end\r\n".
      "SUMMARY:" .  str_replace(",", "\\,", $this->title) . "\r\n".
      "URL:" .  $this->url . "\r\n".
      "DESCRIPTION:" .  $this->description . "\r\n".
      "LOCATION:" . $this->city . "\r\n".
      "END:VEVENT\r\n";
    return $ret;
  }

   /*
   * Function that replaces all xml-incompatible characters with they
   * XML-compatible equivalent
   */
  private function xmlentities($string, $quote_style=ENT_COMPAT)
  {
     return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  }

  function toRss($map)
  {
    $start_day = onlyday_timestamp2humanreadable($this->start);
    $end_day   = onlyday_timestamp2humanreadable($this->end);

    $ret = "  <item rdf:about=\"" .
      calendar_absolute_url("showevent.php?id=" . $this->id) . "\">\n";

    $ret .= "  <title>";
    $ret .= $this->xmlentities($this->city) . " : " . $this->xmlentities($this->title);
    if ($start_day == $end_day)
      $ret .= ", le " . $start_day;
    else
      $ret .= ", du " . $start_day . " au " . $end_day;
    $ret .= "</title>\n";

    $ret .= "   <link>" .
      calendar_absolute_url("showevent.php?id=" . $this->id) . "</link>\n";
    $ret .= "   <dc:identifier>" . $this->id .
      "@agendadulibre.org</dc:identifier>\n";
    $evt_timezone = preg_replace("~^\+([0-9][0-9])([0-9][0-9])$~", "+\\1:\\2",
               date('O', $this->start));
    $evt_date     =str_replace(" ", "T", $this->decision_time);
    $ret .= "   <dc:date>" . $evt_date . $evt_timezone . "</dc:date>\n";
    if ($this->longitude && $this->latitude)
      $ret .= "   <georss:point>" . $this->latitude . " " . $this->longitude . "</georss:point>\n";
    $ret .= "   <description>\n";

    $ret .= $this->xmlentities(strip_tags($this->formatHTML()));

    $ret .= "   </description>\n";
    if (! $map)
      {
        $ret .= "   <content:encoded>\n";
        $ret .= $this->xmlentities($this->formatHTML());
        $ret .= "   </content:encoded>\n";
      }
    $ret .= "  </item>\n\n";

    return $ret;
  }

  function toJs()
  {
    return "document.write(\"".
      "  <li>". strftime ("%d/%m/%Y", $this->start).
      " <a href='".
      calendar_absolute_url("showevent.php?id={$this->id}", "http"). "'>".
      "<b>" . htmlentities(substr(utf8_decode(stripslashes($this->city)),0,15), ENT_COMPAT | ENT_HTML401, 'ISO-8859-15') . "</b>: ".
      htmlentities(substr(utf8_decode(stripslashes($this->title)),0,35), ENT_COMPAT | ENT_HTML401, 'ISO-8859-15').
      "</a>". "</li>\\n\");\n";
  }

    function generate_date_forms ($ident, $current_time)
    {
      echo "<select name=\"__event_" . $ident . "_day\">\n";
      for ($i = 1; $i <= 31; $i++)
        {
          if ($i == date("j", $current_time))
      echo "  <option value=\"$i\" selected=\"selected\">$i</option>\n";
          else
      echo "  <option value=\"$i\">$i</option>\n";
        }
      echo "</select>\n";

      echo "<select name=\"__event_" . $ident . "_month\">\n";
      for ($i = 1; $i <= 12; $i++)
        {
          if ($i == date("n", $current_time))
      echo "  <option value=\"$i\" selected=\"selected\">" . date_month2string($i) . "</option>\n";
          else
      echo "  <option value=\"$i\">" . date_month2string($i) . "</option>\n";
        }
      echo "</select>\n";

      echo "<select name=\"__event_" . $ident . "_year\">\n";
      for ($i = 2005; $i <= (int)date("Y", strtotime('+ 3 years')); $i++)
        {
          if ($i == date("Y", $current_time))
      echo "  <option value=\"$i\" selected=\"selected\">$i</option>\n";
          else
      echo "  <option value=\"$i\">$i</option>\n";
        }
      echo "</select>\n";

      echo "&nbsp;&nbsp;&nbsp;&nbsp;";

      echo "<select name=\"__event_" . $ident . "_hour\">\n";
      for ($i = 0; $i <= 23; $i++)
        {
          if ($i == date("G", $current_time))
      echo "  <option value=\"$i\" selected=\"selected\">$i</option>\n";
          else
      echo "  <option value=\"$i\">$i</option>\n";
        }
      echo "</select>\n";

      echo "&nbsp; h &nbsp;";

      echo "<select name=\"__event_" . $ident . "_minute\">\n";
      for ($i = 0; $i <= 59; $i+=15)
        {
          if ($i == date("i", $current_time))
      echo "  <option value=\"$i\" selected=\"selected\">". sprintf("%02s", $i). "</option>\n";
          else
      echo "  <option value=\"$i\">". sprintf("%02s", $i). "</option>\n";
        }
      echo "</select>\n";
    }

}

?>
