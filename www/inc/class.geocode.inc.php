<?php

/*
 * Copyright 2014-2016
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Function qui prend comme parametre une adresse postale
 * et retourne les coordonnees geographique
 * dans un tableau (latitude, longitude)
 * ou false si l'adresse n'est pas trouvee
 *
 * L'adresse postale est un tableau (address, postalcode, city, country)
 * Cf. http://wiki.openstreetmap.org/wiki/FR:Nominatim#Param.C3.A8tres
 *
 * Se sert de OpenStreetMap via nominatim.
 */

setlocale(LC_CTYPE, $GLOBALS['locale']);
include_once('inc/class.region.inc.php');

class geocode {
    var $place;     // place to find
    var $addresses; // found one or more
    var $error, $message;
    var $db;

    var $osm_type = array('way'=>'W', 'node'=>'N', 'relation'=>'R');

    function geocode($db, $place="")
    {
        $this->db = $db;
        $this->place = $place;
        $address = array();
        $this->error=false;
        $this->message="";
        if ($place!="") {
            $this->findFromPlace();
            return $this;
        } else {
            return $this;
        }
    }

    private function call($query)
    {
    global $moderatorlist;
        $url = "http://nominatim.openstreetmap.org/";
        $url .= /*urlencode*/($query);
        $url .= "&email=". $moderatorlist;
        return simplexml_load_file($url);
    }

    function findFromPlace() {
        $this->place = str_replace("  ", " ", $this->place);
        $this->place = str_replace(", ", ",", $this->place);
        $this->place = str_replace(" ", "+", $this->place);
        $this->place = iconv('UTF-8', 'US-ASCII//TRANSLIT', $this->place);
        $xmlO = $this->call("search?format=xml&addressdetails=1&q=". $this->place);
        if (! $xmlO) {
            $this->message = "Problème pour trouver l'adresse";
            $this->error = true;
            return false;
        }
        if (! isset($xmlO->place)) {
            $this->message = "Aucune adresse trouvée";
            $this->error = true;
            return false;
        }

        $places = array();
        foreach ($xmlO->place as $place) {
            if (isset($place['osm_id'])) {
                $places[] = $place;
            }
        }

        if (count($places)>=10) {
            $this->message = "Trop d'adresses trouvées, soyez plus précis(e)";
            return false;
        }

        $return = array();

        if (count($places)>1) {
            $this->message = "Plusieurs adresses possibles, veuillez sélectionner la bonne";
            foreach ($places as $placeFound) {
                $index = $this->osm_type[(string)$placeFound['osm_type']]. (string)$placeFound['osm_id'];
                $this->addresses[$index] = (string)$placeFound['display_name'];
            }
        }
        else
        {
            // Only one place found.
            $this->fromGeocodePlace($places[0]);
        }
        return $this;
    }

    # http://wiki.openstreetmap.org/wiki/Nominatim#Reverse_Geocoding
    function findFromOsmId($osmId) {
        $xml = $this->call("lookup?format=xml&osm_ids=".$osmId);
        if (! isset($xml->place)) {
            $this->message = "Aucune adresse trouvée par l'identifiant";
            $this->error = true;
            return false;
        }
        //$xml->place[0]['display_name'] = $xml->result;
        $this->fromGeocodePlace($xml->place[0]);
        return $this;
    }

    private function fromGeocodePlace($placeFound)
    {
        $this->addresses[] = array(
            'osm_type'      => $this->osm_type[(string)$placeFound['osm_type']],
            'osm_id'        => (int)$placeFound['osm_id'],
            'lat'           => (float)$placeFound['lat'],
            'lon'           => (float)$placeFound['lon'],
            'display_name'  => (string)$placeFound['display_name'],
            'house_number'  => (string)$placeFound->house_number,
            'road'          => (string)$placeFound->road ,
            'village'       => (string)$placeFound->village ,
            'town'          => (string)$placeFound->town,
            'city'          => (string)$placeFound->city,
            'county'        => (string)$placeFound->county ,
            'state'         => (string)$placeFound->state ,
            'postcode'      => (string)$placeFound->postcode ,
            'country'       => (string)$placeFound->country ,
            'country_code'  => (string)$placeFound->country_code ,
            'address100'    => (string)$placeFound->address100,
        );
        return $this;
    }

    function found2input($inputName)
    {
        $html = "";
        foreach($this->addresses as $placeid=>$adm)
        {
            $html .= "<label for='{$placeid}'>{$adm}&nbsp;<input id='{$placeid}' ".
                "type='radio' name='{$inputName}' value='{$placeid}'></label><br/>\n";
        }
        return $html;
    }

    function toEvent(&$event)
    {
        reset($this->addresses);
        $ad = current($this->addresses);
        $event->place   = $ad['display_name'];
        $event->address = $event->place; //trim($ad['house_number']." ".$ad['road']);
        $event->city    = trim(str_replace("  ", " ",
            ($ad['village']>"" ? $ad['village'] :
                ($ad['town']>"" ? $ad['town'] :
                    ($ad['city']>"" ? $ad['city'] :
                        ($ad['county']>"" ? $ad['county'] : $ad['address100'])
                    )
                )
            )
        ));
        $event->postalcode = ($ad['postcode']>"" ? $ad['postcode'] : "");
        $event->department = ($ad['postcode']>"" ? substr($ad['postcode'],0,2) : "");
        $oRegion = new region($this->db);
        $oRegion->name = $ad['state'];
        $event->region  = ($event->department>0 ? $oRegion->getRegionFromDepartment($event->department) : $oRegion->getFromName());
        $event->country = $ad['country'];
        $event->latitude = $ad['lat'];
        $event->longitude = $ad['lon'];
        $event->osmId = $ad['osm_type']. $ad['osm_id'];
    }

} // end class
