<?php

/*
 * Copyright 2015-2016
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Campagnes d'événements, comme Libre en fête...
 * is_icon = est true s'il faut faire figurer une icone dans l'agenda menuel
 * is_message = est true s'il faut afficher un bandeau important
 * is_tag = est true si un tag est associé à la campagne et des événements peuvent être marqués de cette campagne
 **/

class campaign {
    var $id;
    var $name,                        // nom court de la campagne
        $start, $end,                 // deux dates début et fin de campagne
        $is_message, $messagehtml,        // bandeau important
        $is_icon, $iconhtml,              // icone dans l'agenda mensuel
        $is_tag, $tag, $tag_messagehtml,  // tag associé et message affiché si événement associé à la campagne
        $tag_explainhtml, $tag_explainstart;     // explication du tag lors de la saisie de l'événement,
    // date à partir de laquelle l'explication doit apparaître
    var $is_preview;
    var $error, $message;
    var $db;

    // -------------------------------------------------------------------

    function campaign($db, $id)
    {
        $this->db = $db;
        $this->error = 0;
        $this->message = "";
        if ($id>0)
        {
            $this->id = $id;
            $this->get();
        }
        return ! $this->error;
    }

    // -------------------------------------------------------------------

    function get()
    {
        $query = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}campaign WHERE id=" . $this->db->quote_smart($this->id);
        $result = $this->db->query ($query);
        if (! ($record = $this->db->fetchObject($result)))
        {
            $this->message = "Aucun évènement avec l'ID ". $this->id;
            $this->error = true;
            return false;
        }
        $this->name             = $record->name;
        $this->start            = strtotime($record->start);
        $this->end              = strtotime($record->end);
        $this->is_message       = ($record->is_message == 1);
        $this->messagehtml      = $record->messagehtml;
        $this->is_icon          = ($record->is_icon == 1);
        $this->iconhtml         = $record->iconhtml;
        $this->is_tag           = ($record->is_tag == 1);
        $this->tag              = $record->tag;
        $this->tag_messagehtml  = $record->tag_messagehtml;
        $this->tag_explainhtml  = $record->tag_explainhtml;
        $this->tag_explainstart = strtotime($record->tag_explainstart);

        $this->message = "";
        $this->error = false;
        return true;
    }

    // -------------------------------------------------------------------

    function save()
    {
        $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
        $sql .= "{$GLOBALS['db_tablename_prefix']}campaign SET " .
            "name=" .     $this->db->quote_smart ($this->name)                         . ", ".
            "start=" .    $this->db->quote_smart (date_timestamp2mysql ($this->start)) . ", ".
            "end=" .      $this->db->quote_smart (date_timestamp2mysql ($this->end))   . ", ".
            "is_message=".$this->db->quote_smart ($this->is_message ? 1 : 0)           . ", ".
            "messagehtml=" .     $this->db->quote_smart ($this->messagehtml)           . ", ".
            "is_icon=".   $this->db->quote_smart ($this->is_icon ? 1 : 0)              . ", ".
            "iconhtml=" . $this->db->quote_smart ($this->iconhtml)                     . ", ".
            "is_tag=".    $this->db->quote_smart ($this->is_tag ? 1 : 0)               . ", ".
            "tag=" .      $this->db->quote_smart ($this->tag)                          . ", ".
            "tag_messagehtml=" .      $this->db->quote_smart ($this->tag_messagehtml)  . ", ".
            "tag_explainhtml=" .      $this->db->quote_smart ($this->tag_explainhtml)  . ", ".
            "tag_explainstart=" .    $this->db->quote_smart (date_timestamp2mysql ($this->tag_explainstart));
        if ($this->id!=0)
            $sql .= " WHERE id=" .    $this->db->quote_smart ($this->id);

        $ret = $this->db->query($sql);
        if ($ret == FALSE)
        {
            $this->error=true;
            $this->message = "La requête <i>" . $sql . "</i> a échoué";
            return false;
        }
        if ($this->id==0)
        {
            $this->id = $this->db->insertid();
        }

        return ! $this->error;
    } // end function_save();

    // -------------------------------------------------------------------

    function delete()
    {
        $sql = "DELETE FROM {$GLOBALS['db_tablename_prefix']}campaign WHERE id=" . $this->db->quote_smart($this->id);
        $ret = $this->db->query($sql);
        if (! $ret)
        {
            $this->error=true;
            $this->message = returnError("Impossible de supprimer la campagne");
            error ("La requête <i>" . $sql . "</i> a échoué");
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------

    function messageHtml()
    {
        return $this->is_message ? stripslashes($this->messagehtml) : "";
    }

    // -------------------------------------------------------------------

    function iconHtml()
    {
        return $this->is_icon ? stripslashes($this->iconhtml) : "";
    }

    function tagMessageHtml()
    {
        return $this->is_tag ? stripslashes($this->tag_messagehtml) : "";
    }

    function tagExplainHtml()
    {
        return $this->is_tag ? stripslashes($this->tag_explainhtml) : "";
    }


    // -------------------------------------------------------------------

    /*
     * Form (edit function)
     * $duplicate=true seems get datas but as new record
     */
    function formHtml($duplicate = false)
    {
        $html = "<form action=\"campaigns.php\" method=\"post\"".
            " enctype=\"multipart/form-data\" name=\"campaignForm\">\n".
            "<input type=\"hidden\" name=\"op\" value=\"update\" />\n";
        $html .= "<table class='stats'>";
        if ($this->id>0 && ! $duplicate) {
            $html.= "<input type='hidden' name='camp[id]' value='{$this->id}' />";
            $html.= "<input type='hidden' name='id' value='{$this->id}' />";
        }
        $html .= "<tr class='odd'><td>Nom*</td><th><input type='text' required name='camp[name]' value='{$this->name}' title=\"Saisir un nom de préférence unique qui servira de code pour reconnaître la campagne.\"/></th></tr>\n";
        $html .= "<tr><td>Période*</td><th>".
            "du <input type='date' required name=camp[start] value='". date("d/m/Y", $this->start). "' title=\"Date de début de la campagne, au format MYSQL aaaa-mm-jj.\" />".
            "<br/> au <input type='date' required name=camp[end] value='". date("d/m/Y", $this->end). "' title=\"Date de fin de la campagne, au format jj/mm/aaaa.\" /></th></tr>\n";
        $html .= "<tr class='odd'><td>Message</td><td>".
            "<label for='is_message' title=\"Cocher si un message important doit être affiché pendant la durée de la campagne.\" >Un message ou bandeau doit apparaître <input id='is_message' type='checkbox' name='camp[is_message]' ". ($this->is_message ? "checked" : ""). " value='1' /></label><br/>".
            "<textarea name='camp[messagehtml]' cols=80 rows=6 title=\"Ecrire le code HTML de l'annonce/bandeau qui devra appaître.\">". stripcslashes(htmlentities($this->messagehtml)). "</textarea>".
            "</td></tr>\n";
        $html .= "<tr><td>Icone dans l'agenda</td><td>".
            "<label for='is_icon' title=\"Cocher si une icone doit être affichée pendant la durée de la campagne.\" >Une icone doit apparaître dans l'agenda <input id='is_icon' type='checkbox' name='camp[is_icon]' ". ($this->is_icon ? "checked" : ""). " value='1' /></label><br/>".
            "<textarea name='camp[iconhtml]' cols=80 rows=6 title=\"Ecrire le code HTML de l'icone qui devra appaître dans l'agenda.\">". stripslashes(htmlentities($this->iconhtml)). "</textarea>".
            "</td></tr>\n";

        $html .= "<tr class='odd'><td>Tag associé*</td><td>".
            "<label for='is_tag' title=\"Cocher si un tag est associé à la campagne.\" >Un tag est associé à la campagne <input id='is_tag' type='checkbox' name='camp[is_tag]' ". ($this->is_tag ? "checked" : ""). " value='1' /></label><br/>".
            "Tag&nbsp;: <input type='text' name='camp[tag]' title=\"Saisir le tag associté à la campagne.\" value=\"". $this->tag. "\" />".
            "<br />Message&nbsp;: <textarea name='camp[tag_messagehtml]' cols=80 rows=6 title=\"Ecrire le code HTML du message qui apparaîtra lorsqu'un événement portera le tag.\">". stripslashes(htmlentities($this->tag_messagehtml)). "</textarea>".
            "<br />Explication à la saisie&nbsp;: <textarea name='camp[tag_explainhtml]' cols=80 rows=6 title=\"Ecrire le code HTML du message d'explication lors de la saisie des tags.\">". stripslashes(htmlentities($this->tag_explainhtml)). "</textarea>".
            "<br/>À partir du&nbsp: <input type='date' name=camp[tag_explainstart] value='". date("d/m/Y", $this->tag_explainstart). "' title=\"Date de début d'affichage de l'explication du tag, au format MYSQL aaaa-mm-jj.\" />".
            "</td></tr>\n";

        $html .= "<tr><th></th><th><input type=\"submit\" name=\"camp[preview]\" value=\"". _("Prévisualisation"). "\" /> <input type=\"submit\" name=\"submit\" value=\"". _("Enregistrement"). "\" /></th></tr>\n";
        $html .= "</table>\n";
        $html .= "</form>\n";

        return $html;

    } // end function_fromForm

    // -------------------------------------------------------------------

    /*
     * Form (edit function)
     * $duplicate=true seems get datas but as new record
     */
    function confirmDelFormHtml()
    {
        $html = $this->formatHtml();

        $html .= "<form action=\"campaigns.php\" method=\"post\"".
            " enctype=\"multipart/form-data\" name=\"campaignDelForm\">\n".
            "<input type=\"hidden\" name=\"op\" value=\"delete\" />\n";
        if ($this->id>0) {
            $html.= "<input type='hidden' name='id' value='{$this->id}' />";
        }
        $html .= "<label for='is_del' title=\"Cocher pour confirmer la suppression.\" >Supprimer la campagne <input id='is_del' type='checkbox' name='is_del' value='1' /></label>\n";

        $html .= "<input type=\"submit\" name=\"submit\" value=\"". _("Supprimer"). "\" />\n";
        $html .= "</form>\n";

        return $html;

    }

    // -------------------------------------------------------------------

    /*
     * Came from Form (edit function)
     *
     */
    function fromConfirmDelFormHtml()
    {
        if (isset($_POST['id'])
                && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['id'];
        } else {
            $this->message .= error("Pas de numéro de suppression");
        }

        if (! isset($_POST['is_del']) || $_POST['is_del']!='1') {
            $this->message .= error("Pas de confirmation");
        }

        $this->error = ($this->message>"");

        return ! $this->error;
    }

    // -------------------------------------------------------------------

    /*
     * Came from Form (edit function)
     *
     */
    function fromForm()
    {

        if (isset($_POST['camp']['id'])
                && preg_match("~^[0-9]+$~", $_POST['id'])) { // match only positive integers
            $this->id = $_POST['camp']['id'];
        }

        /* Convert form date to timestamp */
        foreach (array('start', 'end', 'tag_explainstart') as $fieldName)
        {
            if (isset($_POST['camp'][$fieldName]))
            {
                list($y, $m, $d) = explode('/', $_POST['camp'][$fieldName]);
                $this->$fieldName = strtotime("{$y}-{$m}-{$d}");
            }
        }

        /* text */
        foreach (array('name', 'tag') as $fieldName)
        {
            if (isset($_POST['camp'][$fieldName]))
                $this->$fieldName = stripslashes(strip_tags($_POST['camp'][$fieldName]));
        }

        /* html code */
        $aFieldName = array('messagehtml', 'iconhtml', 'tag_messagehtml', 'tag_explainhtml');
        foreach ($aFieldName as $fieldName)
        {
            if (isset($_POST['camp'][$fieldName]))
                $this->$fieldName = stripslashes(strip_tags($_POST['camp'][$fieldName],
                            "<p><strong><em><br/><a><ul><li><ol><b><i><img><h1><h2><h3><code>"));
        }

        /* boolean */
        foreach (array('is_message', 'is_icon', 'is_tag') as $fieldName)
        {
            if (isset($_POST['camp'][$fieldName]))
                $this->$fieldName = ($_POST['camp'][$fieldName]=='1');
        }

        /* preview */
        $this->is_preview = isset($_POST['camp']['preview']);

        return true;

    } // end function_fromForm

    // -------------------------------------------------------------------

    // TODO
    // Check information from event edit form
    function check()
    {
        // bloquants
        if (strlen($this->name)<4)
            $this->message = error("Le nom de la campagne est trop court court");

        if ($this->start > $this->end)
            $this->message = error("La date de fin est avant la date de début");

        if ($this->is_message)
        {
            if (strlen($this->messagehtml)<10)
                $this->message = error("Le message est trop court court");
        }

        if ($this->is_icon)
        {
            if (strlen($this->iconhtml)<10)
                $this->message = error(_("Le code html de l'icone est trop court"));
        }

        if ($this->is_tag)
        {
            if ($this->start < $this->tag_explainstart)
                $this->message = error("La date de début de saisie de tag est après le début de campagne");
            if (strlen($this->tag)<3)
                $this->message = error(_("Tag indispensable d'au moins 3 caractères"));
            if (! preg_match("~^[a-z0-9\-]*$~", $this->tag))
                return error("Le tag ne doit contenir que des lettres minuscules, ".
                        "des chiffres ou des tirets.");
        }

        if ($this->message>"")
            $this->error = true;

        // avertissements non bloquants
        if ($this->start < strtotime("2000-01-01"))
            $this->message = error("La date de début est trop ancienne");

        if ($this->start < strtotime("-1 year", $this->end))
            $this->message = error("La date de fin est plus d'un an après la date de début");

        return !$this->error;
    } //

    // -------------------------------------------------------------------
    function formatHtml()
    {
        $html = "<table class='stats'>";

        $html .= "<tr class='odd'><td>Nom</td><th>{$this->name}</th></tr>\n";
        $html .= "<tr><td>Période</td><th>".
            "du ". onlyday_timestamp2humanreadable($this->start).
            " au ". onlyday_timestamp2humanreadable($this->end). "</th></tr>\n";
        $html .= "<tr class='odd'><td>Message</td><td>".
            ($this->is_message ? "<code>". htmlentities($this->messagehtml). "</code><br/>". $this->messageHtml() : "non").
            "</td></tr>\n";
        $html .= "<tr><td>Icone dans agenda</td><td>".
            ($this->is_icon ? "<code>". htmlentities($this->iconhtml). "</code><br/>". $this->iconHtml() : "non").
            "</td></tr>\n";
        $html .= "<tr class='odd'><td>Tag associé</td><td>";
        if ($this->is_tag)
        {
            $html .= "{$this->tag}<br/>".
                "<table><tr class='even'><td>Message</td><td>". "<code>". htmlentities($this->tag_messagehtml). "</code><br/>". $this->tag_messagehtml. "</td></tr>".
                "<tr><td>Explication à la saisie</td><td>".
                "<code>". htmlentities($this->tag_explainhtml). "</code><br/>". $this->tag_explainhtml. "</td></tr>".
                "<tr class='even'><td>À partir du</td><td>". onlyday_timestamp2humanreadable($this->tag_explainstart). "</td></tr>".
                "</table>";
        } else {
            $html .= "non";
        }
        $html .= "</td></tr>\n";

        $html .= "</table>\n";
        return $html;
    }

    // -------------------------------------------------------------------
}

/****************************************************************/

class campaigns {

    var $campaigns;     // array of campaign
    var $db;

    function campaigns($db)
    {
        $this->campaigns = array();
        $this->db = $db;
        $query = "SELECT id FROM {$GLOBALS['db_tablename_prefix']}campaign".
            " ORDER BY start DESC, end DESC";
        $result = $this->db->query ($query);
        while (($record = $this->db->fetchObject($result)))
        {
            $this->campaigns[$record->id] = new campaign($db, $record->id);
        }
        $this->db->freeResult($result);
        return count($this->campaigns)>0;
    }

    // -------------------------------------------------------------------

    function returnMessage()
    {
        // parcours toutes les campagnes et renvoi la suite des messages correspondant à la date du jour
        $html = "";
        foreach ($this->campaigns as $campaign)
        {
            if ($campaign->start <= strtotime('now') && strtotime('now') <= $campaign->end && $campaign->is_message)
            {
                $html .= $campaign->messageHtml(). "\n";
            }
        }
        return $html;
    }

    // -------------------------------------------------------------------
    // $date doit être au format datetime
    function returnIcon($date)
    {
        // parcours toutes les campagnes et renvoi la suite des messages correspondant à la date du jour
        $html = "";
        foreach ($this->campaigns as $campaign)
        {
            if ($campaign->is_icon && ($campaign->start <= $date) && ($date <= $campaign->end))
            {
                $html .= $campaign->iconHtml(). "\n";
            }
        }
        return $html;
    }

    // -------------------------------------------------------------------

    function returnTagExplain()
    {
        $html = "";
        foreach ($this->campaigns as $campaign)
        {
            if ($campaign->is_tag && ($campaign->tag_explainstart <= strtotime('now')) && (strtotime('now') < $campaign->end))
            {
                $html .= $campaign->tagExplainHtml(). "\n";
            }
        }
        return $html;
    }

    // -------------------------------------------------------------------
    // $tags is a string of tags seperated with space
    function returnTagMessage($tags)
    {
        // renvoie le message des campagnes pour lesquelles l'événement a le tag associé
        $html = "";
        foreach ($this->campaigns as $campaign)
        {
            if ($campaign->is_tag && in_array($campaign->tag, explode(' ', $tags)))
            {
                $html .= $campaign->tagMessageHtml(). "\n";
            }
        }
        return $html;
    }

    // -------------------------------------------------------------------
    function listHtml()
    {
        $html = "<ul>\n";
        foreach ($this->campaigns as $campaign)
        {
            $html .= "  <li><a href='campaigns.php?op=info&id={$campaign->id}'>{$campaign->name}</a> du ".
                onlyday_timestamp2humanreadable($campaign->start).
                " au ". onlyday_timestamp2humanreadable($campaign->end).
                "</li>\n";
        }
        $html .= "</ul>\n";
        return $html;
    }

    // -------------------------------------------------------------------
    function formatHtml($id)
    {
        $campaign = $this->campaigns[$id];
        $html = $campaign->formatHtml();

        $html .= "<p>".
            "<a href='campaigns.php?op=edit&id={$id}'>Editer</a>".
            " - <a href='campaigns.php?op=dupl&id={$id}'>Dupliquer</a>".
            " - <a href='campaigns.php?op=del&id={$id}'>Supprimer</a>".
            "</p>\n";

        return $html;
    }

}

?>
