<?php

/*
 * Copyright 2005-2016
 * - Mélanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * - Loic Dayot <ldayot CHEZ epnadmin POINT net>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

// Test if gettext not installed, define _() fonction
if (! function_exists("_"))
{
  function _($text)
  {
    return $text;
  }
}

// load tags from/to html conversion
include_once("inc/class.tagsHtml.inc.php");
include_once("inc/class.region.inc.php");

/*
 * Compute an absolute URL for the website
 *
 * @param[in] Relative URL in the website
 */
function calendar_absolute_url($url="", $protocol="http")
{
  global $root;
  $root = rtrim ($root, "/");
  return $protocol . "://" . $root . "/" . $url;
}

function calendar_setlocale()
{
  global $locale;
  if(setlocale(LC_TIME, $locale. "fr_FR.utf8", 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8', "fr_FR", "fr_FR@euro") == false)
    {
      error ("Locale setting error");
    }
}

function important_box_html ()
{
global $db;
  include_once('class.campaign.inc.php');
  $campaigns = new campaigns($db);
  $message = $campaigns->returnMessage();

  if ($message>"")
  {
    $return = "<div id=\"important\">\n";
    $return .= $message;
    $return .= "</div>\n";
    return $return;
  }
  return "";
}

function _template_vars($title, $jscriptcode = "", $onloadfunc = "", $portail=false)
{
  global $db;
  global $adl_title, $adl_subtitle, $adl_short_title, $adl_all_region;
  global $adl_title_iframe, $portail;

  /*
   * contents preparation for template
   **/
  $dynamicContent = array();
  $dynamicContent['adl_title']    = $adl_title;
  $dynamicContent['title']        = $title;
  $dynamicContent['javascript']   = $jscriptcode. "\n";
  $dynamicContent['onload']       = ($onloadfunc>"" ? " onload=\"".$onloadfunc."\"" : "");
  $dynamicContent['important_box']= important_box_html();
  $dynamicContent['website_name'] = ($portail ? $adl_title_iframe : $adl_title);
  $dynamicContent['adl_subtitle'] = ($portail ? "" : "<p id=\"subtitle\"><em>{$adl_subtitle}</em></p>");
  $dynamicContent['menu']         = menu_html();
  $dynamicContent['footer_message'] = '';
  /* TODO
   * $dynamicContent['other_countries_box'] = other_countries_box();
   */
  include_once("inc/class.export.inc.php");
  $exportEvent = new exportEvent($db);
  $dynamicContent['rss_feeds']    = $exportEvent->rssFeed();

  return $dynamicContent;
}

function put_header ($title, $jscriptcode = "", $onloadfunc = "")
{
  global $session, $dynamicContent, $portail;

  // Special DUI
  // TODO : généraliser la possibilité de passer en iframe
  if ($session->exists('agenda_libre_portail')) {
    $portail=true;
  } elseif (isset($_REQUEST['portail'])) {
    $session->set("agenda_libre_portail", '1');
    $portail=true;
  } else {
    $portail=false;
  }

  // LD added : force charset
  header("Content-type: text/html; charset=UTF-8");

  $dynamicContent = _template_vars($title, $jscriptcode, $onloadfunc, $portail);

  /*
   * Display template
   */

  /* importing html template */
  if ($portail)
  {
    $htmlTemplate = file_get_contents(_themeFile('template/iframe_header.inc.html'));
  }
  else
  {
    $htmlTemplate = file_get_contents(_themeFile('template/header.inc.html'));
  }
  /* updating dynamic fields */
  $htmlOutput = $htmlTemplate;
  foreach ($dynamicContent as $key => $value) {
    $htmlOutput = str_replace("%$key%", $value, $htmlOutput);
  }

  /* displaying dynamic html */
  print $htmlOutput;

} // end function put_header

function put_footer ()
{
  global $dynamicContent, $portail;

  /*
   * Display template
   */

  /* importing html template */
  if ($portail)
  {
    $htmlTemplate = file_get_contents(_themeFile('template/iframe_footer.inc.html'));
  }
  else
  {
    $htmlTemplate = file_get_contents(_themeFile('template/footer.inc.html'));
  }
  /* updating dynamic fields */
  $htmlOutput = $htmlTemplate;
  foreach ($dynamicContent as $key => $value) {
    $htmlOutput = str_replace("%$key%", $value, $htmlOutput);
  }

  /* displaying dynamic html */
  print $htmlOutput;
  ob_end_flush();
}

function menu_html()
{
  global $session, $user;
  $r = "";
  if ($session->exists("agenda_libre_id"))
  { // moderateur
    $r .= "<a class='list-group-item' href=\"moderation.php\"><i class='fa fa-gavel'></i>&nbsp;Modérer les événements</a>".
      " - <a class='list-group-item' href=\"moderatetags.php\"><i class='fa fa-tags'></i>&nbsp;Gérer les tags</a>".
      " - <a class='list-group-item' href=\"campaigns.php\"><i class='fa fa-book'></i>&nbsp;Gérer les campagnes</a>". "\n";
    if (is_object($user) && $user->is_admin())
    {
      $r .= " - <a class='list-group-item' href=\"moderateusers.php\"><i class='fa fa-users'></i>&nbsp;Gérer les utilisateurs</a>\n";
    }
    $r .= "- <a class='list-group-item' href='moderation.php?disconnect=1'><i class='fa fa-sign-out'></i>&nbsp;Se déconnecter</a><br/>\n";
  }
  $r .= "<a class='list-group-item big' href=\"submit.php\"><i class='fa fa-pencil'></i>&nbsp;Proposer un évènement</a> (publication après modération)<br />\n";
  $r .= "<a class='list-group-item' href=\"index.php\"><i class='fa fa-calendar'></i>&nbsp;Consulter l'agenda</a>";
  $r .= " - <a class='list-group-item' href=\"map.php\"><i class='fa fa-map-marker'></i>&nbsp;Carte</a>";
  $r .= " - <a class='list-group-item' href=\"tags.php\"><i class='fa fa-tags'></i>&nbsp;Mots-clés</a>";
  $r .= " - <a class='list-group-item' href=\"exportlist.php\"><i class='fa fa-external-link'></i>&nbsp;Exporter RSS-iCal-widget</a>";
  $r .= "<br/><a class='list-group-item' href=\"stats.php\"><i class='fa fa-signal'></i>&nbsp;Statistiques</a>";
  $r .= " - <a class='list-group-item' href=\"infos.php\"><i class='fa fa-info'></i>&nbsp;À propos</a>";
  $r .= " - <a class='list-group-item' href=\"contact.php\"><i class='fa fa-envelope'></i>&nbsp;Contact</a>";
  if (! $session->exists("agenda_libre_id"))
  { // moderateur
    $r .= " - <a class='list-group-item' href=\"moderation.php\"><i class='fa fa-sign-in'></i>&nbsp;(réservé)</a>\n";
  }
  /*$r .= "<br />\n";
  $r .= "Exporter&nbsp;: <a class='list-group-item' href=\"rsslist.php\"><i class='fa fa-rss fa-fw'></i>&nbsp;Flux RSS</a>";
  $r .= " - <a class='list-group-item' href=\"icallist.php\"><i class='fa fa-calendar fa-fw'></i>&nbsp;Calendriers iCal</a>";
  $r .= " - <a class='list-group-item' href=\"javascriptlist.php\"><i class='fa fa-code fa-fw'></i>&nbsp;widget (JavaScript)</a>";
  $r .= "\n";*/
  return $r;
}

function scramble_email ($email)
{
  $email = str_replace ("@", " CHEZ ", $email);
  $email = str_replace (".", " POINT ", $email);
  return $email;
}

function error($msg)
{
  echo returnError($msg);
}

function returnError($msg)
{
  return "<p class=\"error\">" . $msg . "</p>";
}

function returnWarning($msg)
{
  return "<p class=\"warning\">" . $msg . "</p>";
}

function date_mysql2timestamp ($mysql_date)
{
  return strtotime ($mysql_date);
}

function date_timestamp2mysql ($timestamp)
{
  return date ("Y-m-d H:i:00", $timestamp);
}

function date_mysql2humanreadable ($mysql_date)
{
  return strftime ("%A %d %B %Y à %Hh%M", date_mysql2timestamp($mysql_date));
}

function date_timestamp2humanreadable($timestamp)
{
  return strftime ("%A %d %B %Y à %Hh%M", $timestamp);
}

function onlyday_timestamp2humanreadable($timestamp)
{
  return strftime ("%A %d %B %Y", $timestamp);
}

function onlyhour_timestamp2humanreadable($timestamp)
{
  return strftime ("%Hh%M", $timestamp);
}

function onlyday_mysql2humanreadable($mysqldate)
{
  return strftime ("%A %d %B %Y", date_mysql2timestamp($mysqldate));
}

function onlyhour_mysql2humanreadable($mysqldate)
{
  return strftime ("%Hh%M", date_mysql2timestamp($mysqldate));
}

function date_month2string($month)
{
  return strftime("%B", strtotime("2010-{$month}-01"));
}

function calendar_mail ($to, $title, $contents, $id = "")
{
  global $debugaddress;
  global $adl_mailto_moderates, $adl_short_title, $root;

  $title = '=?UTF-8?B?'.base64_encode("[$adl_short_title] " . $title).'?=';

  if (isset($debugaddress) && $debugaddress != "")
    $to = $debugaddress;

  return mail($to, $title, $contents,
        /* Message headers */
        "From: {$adl_mailto_moderates}\r\n" .
        "Reply-To: {$adl_mailto_moderates}\r\n" .
        "Content-Type: text/plain; charset=UTF-8\r\n" .
        "Content-Transfert-Encoding: 8bit\r\n" .
        (($id != "") ? ("References: <" . $id . "@{$root}>\r\n") : "") .
        "X-Mailer: ". utf8_decode($adl_short_title). "\r\n"
    );
}

function get_prev_month($month, $year)
{
 if($month == 1)
    {
      $prev_month = 12;
      $prev_year  = $year - 1;
    }
  else
    {
      $prev_month = $month-1;
      $prev_year  = $year;
    }

 return array('year'  => $prev_year,
        'month' => $prev_month);
}

function get_next_month ($month, $year)
{
  if($month == 12)
    {
      $next_month = 1;
      $next_year  = $year + 1;
    }
  else
    {
      $next_month = $month + 1;
      $next_year  = $year;
    }

  return array('year'  => $next_year,
        'month'  => $next_month);
}

function show_day_events ($db, $day, $month, $year, $region, $maxEventsInCalendar=100)
{
  /*
   * Compute timestamp of current day and next day. The next day is
   * needed for the MySQL request to behave properly.
   */
  $cur  = strtotime("$year-$month-$day");
  $next = $cur + (24 * 60 * 60);

    if ($region == "all")
    {
        $sql = "select * from {$GLOBALS['db_tablename_prefix']}events where
            ((end_time >= " . $db->quote_smart(date_timestamp2mysql($cur)) . ") AND
            (start_time <= " . $db->quote_smart(date_timestamp2mysql($next)) . ") AND
            (moderated = 1))";
            }
    else
    {
        $oRegion = new region($db, $region);
        $sql = "SELECT * FROM {$GLOBALS['db_tablename_prefix']}events WHERE
            ((end_time >= " . $db->quote_smart(date_timestamp2mysql($cur)) . ") AND
            (start_time <= " . $db->quote_smart(date_timestamp2mysql($next)) . ") AND
            (moderated = 1) AND
            (department IN (". implode(",", $oRegion->getDepartmentInRegion()). ") OR (locality>=1)) )";
    }
  $result = $db->query ($sql);

  if (! $result)
    {
      echo "Erreur ". $sql;
      return;
    }

  if (($count = $db->numRows ($result)) > 0)
    {
      echo "  <ul>\n";
      $n=0;
      while (($ret = $db->fetchObject ($result)) && ($n<$maxEventsInCalendar))
        {
          echo "   <li><a href=\"showevent.php?id=" . $ret->id . "\">\n";
          echo "    <b>" . str_replace("-", " ", stripslashes($ret->city)) . "</b>: ";
          echo stripslashes($ret->title);
          echo "    </a></li>\n";
          $n++;
        }
      if ($ret)
        {
          $thisDate = onlyday_timestamp2humanreadable($cur);
          echo "<li><a href=\"showoneday.php?date=".
            substr(date_timestamp2mysql($cur),0,10). "\" title=\"".
            sprintf( _("Les %s évènements du %s"), $count, $thisDate ). "\"><b>";

          if ($count-$n==1)
            {
              echo _("+ un autre");
            }
          else
            {
              printf(_("+ %s autres"), $count-$n);
            }
          echo "</b></a></ul>\n";
        }

      echo "  </ul>\n";
    }
}

function calendar_custom_icon($campaigns, $year, $month, $day)
{
  echo $campaigns->returnIcon(strtotime("$year-$month-$day"));
}

function calendar($db, $month, $year, $region)
{
global $maxEventsInCalendar;

  // Chargement des campagnes
  include_once('class.campaign.inc.php');
  $campaigns = new campaigns($db);

  $prev = get_prev_month($month, $year);
  $prev_year  = $prev['year'];
  $prev_month = $prev['month'];

  $next = get_next_month($month, $year);
  $next_year  = $next['year'];
  $next_month = $next['month'];

  $max_day_in_month = date("t",strtotime("$year-{$month}-01"));
  $max_day_in_prev_month = date("t", strtotime("{$prev_year}-{$prev_month}-01"));

  $day = -date("w", strtotime("{$year}-{$month}-01"))+2;

  if ($day > 1)
    $day -= 7;

  echo "<table class=\"calendar\">\n";
  echo " <tr>\n";
  echo "  <th> Lundi </th>\n";
  echo "  <th> Mardi </th>\n";
  echo "  <th> Mercredi </th>\n";
  echo "  <th> Jeudi </th>\n";
  echo "  <th> Vendredi </th>\n";
  echo "  <th> Samedi </th>\n";
  echo "  <th> Dimanche </th>\n";
  echo " </tr>\n";

  for($iter_week = 0; $iter_week < 6 ; $iter_week++)
    {
      echo " <tr>\n";
      for($iter_day = 0; $iter_day < 7; $iter_day++)
  {
    /* Show days before the beginning of the current month
             (previous month)  */
    if($day <= 0)
      {
        echo "  <td class=\"other_month\">\n";
        echo "   <h1>";
        calendar_custom_icon($campaigns, $year, $prev_month, $max_day_in_prev_month + $day);
        echo $max_day_in_prev_month + $day;
        echo "</h1>\n";
        show_day_events ($db, $max_day_in_prev_month + $day, $prev_month, $prev_year, $region, $maxEventsInCalendar);
        echo "  </td>\n";
      }
    /* Show days of the current month */
    elseif(($day <= $max_day_in_month))
      {
              $curtime = mktime(0,0,0, $month, $day, $year);

              if ($curtime < mktime(0,0,0))
    echo "  <td class=\"past_month\"><h1>";
        else if($curtime == mktime(0,0,0))
    echo "  <td class=\"current_month today\"><h1>";
        else
    echo "  <td class=\"current_month\"><h1>";
        calendar_custom_icon($campaigns, $year, $month, $day);
        echo $day;
        echo "</h1>\n";
        show_day_events ($db, $day, $month, $year, $region, $maxEventsInCalendar);
        echo "</td>\n";
      }
    /* Show days after the current month (next month) */
    else
      {
        echo "  <td class=\"other_month\"><h1>";
        calendar_custom_icon($campaigns, $year, $next_month, $day - $max_day_in_month);
        echo $day - $max_day_in_month;
        echo "</h1>\n";
        show_day_events ($db, $day - $max_day_in_month, $next_month, $next_year, $region, $maxEventsInCalendar);
        echo "</td>\n";
      }
    $day++;
  }
      echo " </tr>\n";
    }
  echo "</table>\n";
}

function has_event_in_past($db, $month, $year, $region)
{
  $date = $year . "-" . $month . "-" . "01";
    if ($region == "all")
    {
        $req = $db->query ("SELECT id FROM {$GLOBALS['db_tablename_prefix']}events ".
            "WHERE moderated=1 AND start_time < '" . $date . "'");
    }
    else
    {
        $oRegion = new region($db, $region);
        $req = $db->query ("SELECT id FROM {$GLOBALS['db_tablename_prefix']}events ".
            "WHERE moderated=1 AND start_time < '". $date. "' ".
            "AND department IN (". implode(",", $oRegion->getDepartmentInRegion()). ")");
    }

    return ($db->numRows($req) != 0);
}

function has_event_in_future($db, $month, $year, $region)
{
    $date = $year . "-" . $month . "-" . "01";
    if ($region == "all")
    {
        $req = $db->query ("SELECT id FROM {$GLOBALS['db_tablename_prefix']}events ".
            "WHERE moderated=1 AND end_time >= '" . $date . "'");
    }
    else
    {
        $oRegion = new region($db, $region);
        $req = $db->query ("SELECT id FROM {$GLOBALS['db_tablename_prefix']}events ".
            "WHERE moderated=1 AND end_time >= '". $date. "' ".
            "AND department IN (". implode(",", $oRegion->getDepartmentInRegion()). ")");
    }

    return ($db->numRows($req) != 0);
}

function one_month_calendar($db, $month, $year, $region)
{
  /*
   * Compute previous and next months
   */
  $next = get_next_month($month, $year);
  $next_year  = $next['year'];
  $next_month = $next['month'];

  $prev = get_prev_month($month, $year);
  $prev_year  = $prev['year'];
  $prev_month = $prev['month'];

  /*
   * Display the month calendar header, which contains:
   *  1. Link to the previous month
   *  2. Name of the current month
   *  3. Current year, with link to the corresponding year calendar
   *  4. Link to the next month
   *  5. The region selector
   */
  echo "<h2 class=\"calendar\">\n";
  if (has_event_in_past($db, $month, $year, $region))
    echo " <a href=\"?year=".$prev_year."&amp;month=".$prev_month."&amp;region=".$region."\" title=\"". date_month2string($prev_month)." ".$prev_year. "\"> &lt;&lt; </a>\n";
  echo " <span id=\"month_name\">".date_month2string($month)." ";
  echo " <a href=\"?year=".$year."&amp;region=".$region."\">".$year."</a>"."</span>\n";
  if (has_event_in_future($db, $next_month, $next_year, $region))
    echo " <a href=\"?year=".$next_year."&amp;month=".$next_month."&amp;region=".$region."\" title=\"". date_month2string($next_month)." ".$next_year. "\"> &gt;&gt; </a>\n";

  echo "</h2>\n\n";

  echo "<form method='get' action='index.php?year=" . $year . "&amp;month=".$month ."' class='calendar'><p>\n";
  echo "<input type ='hidden' name='year' value='$year' /><input type ='hidden' name='month' value='$month' />\n";

  echo "évènements de ";

  $regionO = new region($db, $region);
  echo $regionO->selectHTML("onchange=\"window.location='?year=" . $year . "&amp;month=" .
    $month . "&amp;region=' + this.options[this.selectedIndex].value\"");
  if ($region!="all")
    {
      echo " et à portée nationale ou internationale";
    }
  echo " <input type='submit' value='". _("Ok") ."' /></p>";
  echo "</form>";

  /*
   * Display the calendar
   */
  calendar($db, $month, $year, $region);

  echo "<p id=\"notice\">Ce calendrier en ";
  echo "<a href=\"" . calendar_absolute_url("rss.php?region=" . $region, "http"). "\">rss</a>, ";
  echo "<a href=\"" . calendar_absolute_url("ical.php?region=" . $region, "webcal") . "\">iCal</a>, ";
  echo "<a href=\"" . calendar_absolute_url("javascript.php?region=" . $region, "http") . "\">JavaScript</a> ou ";
  echo "<a href=\"http://www.google.com/calendar/render?cid=" . calendar_absolute_url("ical.php?region%3D" . $region, "http") . "\">calendrier Google</a>";
  echo "</p>";
}

function year_calendar($db, $year, $region)
{
  /*
   * Display the year calendar header, which contains:
   *  1. Link to the previous year
   *  3. Current year
   *  4. Link to the next year
   *  5. The region selector
   */

  $next_year  = $year + 1;
  $prev_year  = $year - 1;
  echo "<h2 class=\"calendar\">\n";
  if (has_event_in_past($db, 1, $year, $region))
    echo " <a href=\"?year=".$prev_year."&amp;region=".$region."\" title=\"". $prev_year. "\"> &lt;&lt; </a>\n";
  echo " <span id=\"month_name\">".$year."</span>\n";
  if (has_event_in_future($db, 1, $next_year, $region))
    echo " <a href=\"?year=".$next_year."&amp;region=".$region."\" title=\"". $next_year. "\"> &gt;&gt; </a>\n";
  echo "</h2>\n";

  echo "<form class='calendar'><p>";
  echo "<input type ='hidden' name='year' value='$year' />\n";

  $regionO = new region($db, $region);
  echo $regionO->selectHTML("onchange=\"window.location='?year=" . $year .
    "&amp;region=' + this.options[this.selectedIndex].value\"");
  if ($region!="all")
    {
      echo " et à portée nationale ou internationale";
    }
  echo " <input type='submit' value='". _("Ok") ."' /></p>";

  echo "</form>";

  /*
   * Display the calendar over 12 months starting from current one
   */
  $num = 0;
  $month = 1; // start from January
  while ($num < 12) {
    // month header, with a link to the single month on a page
    echo "<h3 class=\"calendar\">\n";
    echo " <a href=\"?year=".$year."&amp;month=".$month."&amp;region=".$region."\">\n";
    echo "  <span id=\"month_name\">".date_month2string($month)." ".$year."</span>\n";
    echo " </a>\n";
    echo "</h3>\n\n";

    calendar($db, $month, $year, $region);

    $next = get_next_month($month, $year);
    $year = $next['year'];
    $month = $next['month'];
    $num++;
  }

  echo "<p id=\"notice\">Ce calendrier en ";
  echo "<a href=\"rss.php?region=" . $region . "\">rss</a>, ";
  echo "<a href=\"" . calendar_absolute_url("ical.php?region=" . $region, "webcal") . "\">iCal</a> ou ";
  echo "<a href=\"http://www.google.com/calendar/render?cid=" . calendar_absolute_url("ical.php?region%3D" . $region, "http") . "\">calendrier Google</a>";
  echo "</p>";
}

/* Function to safely get identifiers from forms */
function get_safe_integer($name, $default)
{
  if (isset($_GET[$name]) && $_GET[$name]>"" && preg_match("~^[0-9]+$~", $_GET[$name])) { // match only positive integers
    return $_GET[$name];
  } else {
    return $default;
  }
}

function get_safe_hash($name, $default)
{
  if (isset($_GET[$name]) && $_GET[$name]>"" && preg_match("~^[0-9a-f]+$~", $_GET[$name])) {
    return $_GET[$name];
  } else {
    return $default;
  }
}

function get_safe_string($name, $default)
{
  if (isset($_GET[$name]) && $_GET[$name]>"" && preg_match("~^[a-z0-9\-\ ]*$~", $_GET[$name]))
    return $_GET[$name];
  else
    return $default;
}

return;
?>
