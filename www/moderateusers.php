<?php

/*
 * Copyright 2014-2015
 * - Loic Dayot <ldayot CHEZ ouvaton POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = true;

include("inc/main.inc.php");

put_header(_("Gestion des utilisateurs/modérateurs"));

echo "<h2>Gérer les utilisateurs/modérateurs</h2>\n";


/*
 *
 * Main page
 *
 */

$op = (isset($_REQUEST["op"]) ? $_REQUEST["op"] : null);

switch (isset($_POST['op']) ? $_POST['op'] : '') // compute
{

  case "update" :
  {
    $user0 = new user($db, $session);
    if ($user0->fromForm())
    {
      if ($user0->check())
      {
        if ($user0->save())
        {
          header("Location:" . calendar_absolute_url('moderateusers.php?op=updated'));
        }
      }
    }
    echo $user0->message;
    if (isset($_POST['__user_id'])) 
    {
      $_REQUEST['id'] = $_POST['__user_id'];
      $op = 'edit';
    } 
    else 
    {
      $op = 'new';
    }
    break;
  }

  case "delete" :
  {
    $user0 = new user($db, $session);
    if ($user0->fromForm())
    {
      if ($user0->checkDel())
      {
        if ($user0->delete())
        {
          header("Location:" . calendar_absolute_url('moderateusers.php?op=updated'));
        }
      }
    }
    echo $user0->message;
    break;
  }

} // end switch compute


switch ($op) // display
{

  case "del" :
  {
    $user0 = new user($db, $session);
    $user0->id = $_REQUEST["id"];
    $user0->get();
    $user0->delForm();
    break;
  }

  case "first" :
  {
    echo returnError("Première utilisation, il faut créer un compte pour l'administration et la modération.");
  }

  case "new" :
  {
    $user0 = new user($db, $session);
    $user0->edit();
    break;
  }

  case "edit" :
  {
    $user0 = new user($db, $session);
    $user0->id = $_REQUEST["id"];
    $user0->get();
    $user0->edit();
    break;
  }

  case 'updated' :
  {
    echo "<p>Données enregistrées</p>\n";
  }

  default :
  case "list" :
  {
    echo $user->listHTML();
    echo "<p><a href='?op=new'>Ajouter un compte utilisateur/modérateur</a></p>\n";
    break;
  }

} // end switch display

if ($op!="" && $op!="list")
  echo "<p><a href=\"moderateusers.php\">".
   _("Liste des utilisateurs/modérateurs"). "</a></p>\n";

put_footer();

?>
