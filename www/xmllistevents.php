<?php

/*
 * Copyright 2007-2015
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 *
 * This file is part of agenda-libre-php.
 *
 * agenda-libre-php is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * agenda-libre-ph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with agenda-libre-php.  If not, see <http://www.gnu.org/licenses/>.
 */

$privatePage = false;

include("inc/main.inc.php");

if (isset($_GET['tag']) && preg_match("~^[a-z0-9\-]*$~", $_GET['tag']))
	$tag = $_GET['tag'];
else
	$tag = '';

if (isset($_GET['start']) && $_GET['start'] == 'now')
	$start = "now";
else
	$start = '';

$limit = get_safe_integer('limit', 0);

/*$sql = "select * from {$GLOBALS['db_tablename_prefix']}events where (tags like '%" . $_GET['tag'] . "%') " .
       "and (moderated=1) order by start_time";*/

$sql = "select * from {$GLOBALS['db_tablename_prefix']}events where (moderated=1)";

if ($start == 'now')
	$sql .= " and (end_time >= '" . date_timestamp2mysql(time()) . "')";

if ($tag != '')
	$sql .= " and (concat(' ', tags, ' ') like '% " . $tag . " %') ";

$sql .= " order by start_time asc";

if ($limit != 0)
	$sql .= " limit " . $limit;

$events = $db->query($sql);
if (! $events)
{
  echo "Erreur lors de la requête SQL.";
  exit;
}

$xml = new xmlWriter();
$xml->openMemory();
$xml->startDocument("1.0", "UTF-8");

$xml->startElement("events");

while($event = mysql_fetch_object($events))
{
  $xml->startElement("event");
  $xml->writeElement("title", $event->title);
  $xml->writeElement("start-time", $event->start_time);
  $xml->writeElement("end-time", $event->end_time);
  $xml->writeElement("city", $event->city);
  $xml->writeElement("region", region_find($db, $event->region));
  $xml->writeElement("locality", $event->locality);
  $xml->writeElement("url", $event->url);
  $xml->writeElement("tags", eventTagsList($event->id));
  $xml->writeElement("contact", $event->contact);
  $xml->writeElement("adlurl", calendar_absolute_url("showevent.php?id=" . $event->id));
  $xml->startElement("description");
  $xml->writeCData($event->description);
  $xml->endElement();
  $xml->endElement();
}

$xml->EndElement();
$xml->endDocument();

header("Content-type: text/xml");
echo $xml->outputMemory(true);

?>
