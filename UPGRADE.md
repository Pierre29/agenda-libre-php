# upgrade guide

This guide will explain how to upgrade **agenda-libre-php** from a previous version. The upgrade has been tested on a freshly installed Debian Jessie server with **agenda-libre-php** installed following the install guide [INSTALL.md](INSTALL.md).

## usual upgrade

### upgrading the source code

First, the best thing will be to backup your whole installation (files and database). But you have an automatic daily backup, don't you? :-)

The usual upgrade is similar from a usual install. 

We will mostly paraphrase the two section of the install guide [INSTALL.md](INSTALL.md):

- [get agenda-libre-php source code](INSTALL.md#get-agenda-libre-php-source-code)
- [install in a directory accessible to apache](INSTALL.md#install-in-a-directory-accessible-to-apache)

```bash
mkdir -p ~/tmp/agenda-libre-php/
cd ~/tmp/agenda-libre-php/
wget https://git.framasoft.org/agenda-libre/agenda-libre-php/repository/archive.zip
unzip archive.zip
cp -a ~/tmp/agenda-libre-php/agenda-libre-php*/www/* /var/www/html/
rm -rf ~/tmp/agenda-libre-php/
```

Your configuration in `/var/www/html/inc/config.inc.php` and any custom theme (such as `/var/www/html/my-new-theme/`) won't be crushed by the upgrade.

This upgrade snippet assumes that your instance of **agenda-libre-php** is installed into `/var/www/html/`. Change this path if it's installed elsewhere.

### grant mandatory database permissions

If you followed the [install guide](INSTALL.md), your application has no rights to apply [ddl statements](https://en.wikipedia.org/wiki/Data_definition_language). You then have to grant your user some ddl permissions for the upgrade process only (you will have to revoke these permissions after upgrade process).

```bash
mysql --user=root --password
Enter password:

[...]

MariaDB [(none)]> GRANT CREATE, ALTER, DROP ON agendadb.* TO 'agendauser'@'localhost';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> quit
Bye
```

### upgrading the database schema

Sometime, a new version needs that some changes are made to the database schema. This is a `BREAKING CHANGE` and is notified in the [CHANGELOG.md](CHANGELOG.md)

If there is one or more `BREAKING CHANGE` about database schema between your current version and the version you want to install, open a session to the MariaDB server and *copy/paste* the sql statement(s) in order to update or alter to database schema :

```bash
mysql --user=agendauser --password
Enter password:

[...]

MariaDB [(none)]> USE agendadb;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed

MariaDB [agendadb]> <sql-statement(s)-here>
Query OK, 0 rows affected (0.00 sec)

MariaDB [agendadb]> quit
Bye
```

### revoke unnecessary database permissions

Once the schema update is done, your application do not need the ddl permissions any more. You can now revoke the unnecessary database permissions.

```bash
mysql --user=root --password
Enter password:

[...]

MariaDB [(none)]> REVOKE CREATE, ALTER, DROP ON agendadb.* FROM 'agendauser'@'localhost';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> quit
Bye
```

## upgrading from an older version

Versions prior to **3.0.0** were not managed the same way, hence the [CHANGELOG.md](CHANGELOG.md) does not mention the `BREAKING CHANGE`.

If you're upgrading from a version prior to **3.0.0**, please upgrade to version [3.1.0](https://git.framasoft.org/agenda-libre/agenda-libre-php/tree/3.1.0) as a prerequisite. Follow upgrade instructions from [this upgrade guide](https://git.framasoft.org/agenda-libre/agenda-libre-php/blob/3.1.0/UPGRADE.md). Once this is done, upgrade to the last stable version.
